<h1>Instrucciones Generales</h1>
<p>
En este apartado usted podrá revisar sus <strong>PROYECTOS</strong> dando click en el menú de arriba en <strong>"VER PROYECTOS"</strong>, 
los cuales pueden brindarle informacion de los <strong>EQUIPOS</strong> 
instalados en dichos proyectos.
</p>
<br>

<h3 style="color: gray">-- Sobre los PROYECTOS</h3>
<p>
Como cliente, usted puede tener asignados UNO o VARIOS PROYECTOS y 
cada PROYECTO le presentará una visual de los EQUIPOS involucrados en su proyecto.
</p>
<br>

<h3 style="color: gray">---- Sobre los EQUIPOS</h3>
<p>
Cada EQUIPO presenta informacion valiosa respecto al mismo
además de una fotografía de Guía solo como referencia
que puede variar del original.
</p>
<br>

<h3 style="color: gray">------ Sobre las PRUEBAS</h3>
<p>
Las PRUEBAS son realizadas a CADA EQUIPO en particular.
Para ver todas las pruebas de un determinado equipo, ingrese a dicho EQUIPO desde 
la ventana de PROYECTO o haciendo CLICK sobre el nombre del EQUIPO en una tabla de INFORME.
</p>

<h3 style="color: gray">---- Sobre los INFORMES</h3>
<p>
Cada informe le presentará datos IMPORTANTES de UN PROYECTO, 
basado en VARIAS PRUEBAS realizadas a sus equipos.
Para ver cada equipo individual haga click sobre el NOMBRE de dicho Equipo.
</p>
<br>