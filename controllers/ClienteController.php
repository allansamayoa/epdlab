<?php
require_once 'models/cliente.php';

class clienteController{

    public function index(){
        echo "Controlador de Clientes, Acción index.";
    }

    public function gestionar(){
        $cliente = new Cliente();
        $clientes = $cliente->getAll();

        require_once 'views/clientes/gestionar.php';
    }

    public function login(){
        if(isset($_POST)){
            //Identificar al usuario
            //Consulta a la base de datos
            var_dump($_POST);
            die();
            
            $cliente = new Cliente();
            $cliente->setEmail($_POST['email']);
            $cliente->setPassword($_POST['password']);
            
            $identity = $cliente->login();

            //Crear una sesion
            if($identity && is_object($identity)){
                $_SESSION['identity'] = $identity;
                header("Location:".base_url."usuario/sesionIdent");
            }else{
                $_SESSION['error_login'] = 'Identificacion Fallida';
            }

        }
        header("Location:".base_url."proyecto/gestionar");
    }

    public function registro(){
        require_once 'views/clientes/registro.php';
    }
/*
    private $id;
    private $admin_id;
    private $nombre;
    private $apellidos;
    private $email;
    private $password;
    private $empresa;
    private $ciudad;
    private $vendedor;
    private $fecha_reg;

    */

    public function save(){
        if(isset($_POST)){
 
            $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
            $apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;
            $email = isset($_POST['email']) ? $_POST['email'] : false;
            $password = isset($_POST['password']) ? $_POST['password'] : false;
            $empresa = isset($_POST['empresa']) ? $_POST['empresa'] : false;
            $ciudad = isset($_POST['ciudad']) ? $_POST['ciudad'] : false;
            $vendedor = isset($_POST['vendedor']) ? $_POST['vendedor'] : false;

            if($nombre && $apellidos && $email && $empresa && $ciudad){

                $admin_id = $_SESSION['identity']->id;
                $rol = "cliente";

                $cliente = new Cliente();
                $cliente->setAdmin_id($admin_id);
                $cliente->setNombre($nombre);
                $cliente->setApellidos($apellidos);
                $cliente->setEmail($email);
                $cliente->setPassword($password);
                $cliente->setRol($rol);
                $cliente->setEmpresa($empresa);
                $cliente->setCiudad($ciudad);
                $cliente->setVendedor($vendedor);

                if(isset($_GET['id'])){
                    $id = $_GET['id'];
                    
                    $cliente->setId($id);
                    $save = $cliente->edit();
                }else{
                    $save = $cliente->save();
                }

                if($save){
                    $_SESSION['register'] = "complete";
                }else{
                    $_SESSION['register'] = "failed";
                }
            }else{
                $_SESSION['register'] = "failed";
            }
        }else{
            $_SESSION['register'] = "failed";
        }
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            header("Location:".base_url.'cliente/gestionar');
        }else{
            header("Location:".base_url.'cliente/gestionar');
        }

        
    }

    
    public function eliminar(){
        Utilities::isAdmin();

        if(isset($_GET['id'])){
            $id = $_GET['id'];

            $cliente = new Cliente();
            $cliente->setId($id);

            $delete = $cliente->delete();

            if($delete){
                $_SESSION['delete'] = 'complete';
            }else{
                $_SESSION['delete'] = 'failed';
            }
        }else{
            $_SESSION['delete'] = 'failed';
        }
        header("Location:".base_url.'cliente/gestionar');
    }

    public function editar(){
        if(isset($_GET['id'])){
            
            $id = $_GET['id'];

            $edit = true;

            $cliente = new Cliente();
            $cliente->setId($id);
            
            $cliente = $cliente->getOneByID(); 
            
            require_once 'views/clientes/registro.php';
        }else{
            header("Location:".base_url.'clientes/registro&id=');
        }

    }

} // FIN CLASE