<?php
require_once 'models/prueba.php';

class pruebaController{

    public function index(){
        echo "Controlador de Pruebas, Acción index.";
    }

    public function gestionar(){
        Utilities::isAdmin();

        $prueba = new Prueba();
        $pruebas = $prueba->getAll();

        require_once 'views/pruebas/gestionar.php';
    }

    
    public function registrar(){
        Utilities::isAdmin();


        require_once 'views/pruebas/registrar.php';
    }

    public function registrarByEqui(){
        Utilities::isAdmin();


        require_once 'views/pruebas/registrarByEqui.php';
    }
   
    public function save(){
        Utilities::isAdmin();
        if(isset($_POST)){
            $equipo_id = isset($_POST['equipo']) ? $_POST['equipo'] : false;
            $nombre_elec = isset($_POST['nombre_elec']) ? $_POST['nombre_elec'] : false;
            $nombre_ace = isset($_POST['nombre_ace']) ? $_POST['nombre_ace'] : false;
            $tipo_prueba = isset($_POST['tipo']) ? $_POST['tipo'] : false;
            $resultado = isset($_POST['resultado']) ? $_POST['resultado'] : false;
            $recomend = isset($_POST['recomend']) ? $_POST['recomend'] : false;
            $fecha_prueba = isset($_POST['fecha_prueba']) ? $_POST['fecha_prueba'] : false;
            //$imagen = isset($_POST['imagen']) ? $_POST['imagen'] : false;
            if($tipo_prueba == 1){
                $nombre = $nombre_elec;
            }else{
                $nombre = $nombre_ace;
            }

            if($equipo_id && $nombre && $tipo_prueba && $resultado && $recomend && $fecha_prueba){

                $admin_id = $_SESSION['identity']->id;
                $abreviacion = "ABC";

                $recomend = empty($recomend) || $recomend=='' ? 'N/A' : $recomend;

                $prueba = new Prueba();
                $prueba->setEquipo_id($equipo_id);
                $prueba->setNombre($nombre);
                $prueba->setAbreviacion($abreviacion);
                $prueba->setTipo_prueba($tipo_prueba);
                $prueba->setResultado($resultado);
                $prueba->setRecomend($recomend);
                $prueba->setFecha_prueba($fecha_prueba);

               //Guardar EL ARCHIVO
                $archivo = $_FILES['archivo'];
                $filename = $archivo['name'];
                $mimetype = $archivo['type'];

                if($mimetype == "application/pdf"){
                    
                    if(!is_dir('uploads/pruebas')){
                        mkdir('uploads/pruebas', 0777, true);
                    }

                    $prueba->setArchivo($filename);
                    move_uploaded_file($archivo['tmp_name'], 'uploads/pruebas/'.$filename);
                }

                if(isset($_GET['id'])){
                    $id = $_GET['id'];

                    /*var_dump($id);
                    echo "Estoy en EDIT";
                    die();*/


                    $prueba->setId($id);

                    $save = $prueba->edit();
                }else{
                    
                    /*var_dump($_GET['id']);
                    echo "Estoy en SAVE";
                    die();*/

                    $save = $prueba->save();
                }

                if($save){
                    $_SESSION['prueba'] = "complete";
                }else{
                    $_SESSION['prueba'] = "Failed";
                }
            }else{
                $_SESSION['prueba'] = "Failed";
            }
        }else{
            $_SESSION['prueba'] = "Failed";
        }           
        $id = $_GET['id'];
        header("Location:".base_url."proyecto/show&id=".$id);
    }

    public function saveByEquipo(){
        Utilities::isAdmin();
        if(isset($_POST) && isset($_GET)){

            $equipo_id = isset($_GET['equid']) ? $_GET['equid'] : false;
            $nombre_elec = isset($_POST['nombre_elec']) ? $_POST['nombre_elec'] : false;
            $nombre_ace = isset($_POST['nombre_ace']) ? $_POST['nombre_ace'] : false;
            $tipo_prueba = isset($_POST['tipo']) ? $_POST['tipo'] : false;
            $resultado = isset($_POST['resultado']) ? $_POST['resultado'] : false;
            $recomend = isset($_POST['recomend']) ? $_POST['recomend'] : false;
            $fecha_prueba = isset($_POST['fecha_prueba']) ? $_POST['fecha_prueba'] : false;
            

            if($tipo_prueba == 1){
                $nombre = $nombre_elec;
                $cpru = Utilities::showCurrentNamePrueba($nombre);
                
            }else{
                $nombre = $nombre_ace;
                $cpru = Utilities::showCurrentNamePrueba($nombre);
                
            }

            $nombre = $cpru->nombre;

            if($equipo_id && $nombre && $tipo_prueba && $resultado && $fecha_prueba){

                $admin_id = $_SESSION['identity']->id;
                $abreviacion = "$cpru->abreviacion";

                $recomend = empty($recomend) || $recomend=='' ? 'N/A' : $recomend;

                $prueba = new Prueba();
                $prueba->setEquipo_id($equipo_id);
                $prueba->setNombre($nombre);
                $prueba->setAbreviacion($abreviacion);
                $prueba->setTipo_prueba($tipo_prueba);
                $prueba->setResultado($resultado);
                $prueba->setRecomend($recomend);
                $prueba->setFecha_prueba($fecha_prueba);



                //Guardar EL ARCHIVO
                $archivo = $_FILES['archivo'];
                $filename = $archivo['name'];
                $mimetype = $archivo['type'];

                if($mimetype == "application/pdf"){
                    
                    if(!is_dir('uploads/pruebas')){
                        mkdir('uploads/pruebas', 0777, true);
                    }

                    $prueba->setArchivo($filename);
                    move_uploaded_file($archivo['tmp_name'], 'uploads/pruebas/'.$filename);
                }

                if(isset($_GET['id'])){
                    $id = $_GET['id'];

                    $prueba->setId($id);

                    $save = $prueba->edit();
                }else{
                    
                    $save = $prueba->save();
                }

                if($save){
                    $_SESSION['prueba'] = "complete";
                }else{
                    $_SESSION['prueba'] = "Failed";
                }
            }else{
                $_SESSION['prueba'] = "Failed";
            }
        }else{
            $_SESSION['prueba'] = "Failed";
        }           
        $equid = $_GET['equid'];
        $proid = $_GET['proid'];
        header("Location:".base_url."equipo/show&id=".$equid."&proid=".$proid);
    }

    public function pruebaByEquipo(){
        Utilities::isAdmin();

        $id = $_GET['id'];

        $prueba = new Prueba();
        $prueba->setEquipo_id($id);
        $prueba = $prueba->getSomeByEquipoId();
        /*var_dump($informes);
        die();
        */
        require_once 'views/pruebas/pruebasByEquipo.php';
    }

    public function eliminar(){
        Utilities::isAdmin();

        if(isset($_GET['id'])){
            $id = $_GET['id'];

            $prueba = new Prueba();
            $prueba->setId($id);

            $delete = $prueba->delete();

            if($delete){
                $_SESSION['delete'] = 'complete';
            }else{
                $_SESSION['delete'] = 'failed';
            }
        }else{
            $_SESSION['delete'] = 'failed';
        }
        $proid = $_GET['proid'];
        $equid = $_GET['equid'];
        header("Location:".base_url."equipo/show&id=".$equid."&proid=".$proid);
    }
    
    public function editar(){
        if(isset($_GET['id']) && isset($_GET['equid']) && isset($_GET['proid'])){
            
            $id = $_GET['id'];
            $equid = $_GET['equid'];
            $proid = $_GET['proid'];

            $edit = true;

            $prueba = new Prueba();
            $prueba->setId($id);
            
            $prueba = $prueba->getOneByID(); 
            
            require_once 'views/pruebas/registrarByEqui.php';
        }else{
            header("Location:".base_url.'equipo/show&id=');
        }

    }

} // FIN CLASE