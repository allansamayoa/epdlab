<?php
require_once 'models/equipo.php';

class equipoController{

    public function index(){
        echo "Controlador de Equipos, Acción index.";
    }

    public function gestionar(){
        Utilities::isAdmin();

        $equipo = new Equipo();
        $equipos = $equipo->getAll();

        require_once 'views/equipos/gestionar.php';
    }

    public function agregar(){
        Utilities::isAdmin();


        require_once 'views/equipos/agregar.php';
    }

    public function save(){
        Utilities::isAdmin();
        if(isset($_POST) && isset($_GET)){
            $proyecto_id = isset($_GET['proid']) ? $_GET['proid'] : false;
            $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
            $modelo = isset($_POST['modelo']) ? $_POST['modelo'] : false;
            $serie = isset($_POST['serie']) ? $_POST['serie'] : false;
            $fabricante = isset($_POST['fabricante']) ? $_POST['fabricante'] : false;
            $descripcion = isset($_POST['descripcion']) ? $_POST['descripcion'] : false;
            //$imagen = isset($_POST['imagen']) ? $_POST['imagen'] : false;


            if($proyecto_id && $nombre && $modelo && $serie && $fabricante && $descripcion){

                $admin_id = $_SESSION['identity']->id;
                
                $equipo = new Equipo();
                $equipo->setProyecto_id($proyecto_id);
                $equipo->setNombre($nombre);
                $equipo->setMarca($fabricante);
                $equipo->setModelo($modelo);
                $equipo->setSerie($serie);
                $equipo->setFabricante($fabricante);
                $equipo->setDescripcion($descripcion);

                //Guardar la Imagen
                    if(isset($_FILES['imagen'])){
                        $archivo = $_FILES['imagen'];
                        $filename = $archivo['name'];
                        $mimetype = $archivo['type'];
        
                        if($mimetype == "image/jpg"  || $mimetype == "image/jpeg" || $mimetype == "image/png" || $mimetype == "image/gif"){
                            
                            if(!is_dir('uploads/images')){
                                mkdir('uploads/images', 0777, true);
                            }
        
                            $equipo->setImagen($filename);
                            move_uploaded_file($archivo['tmp_name'], 'uploads/images/'.$filename);
                    }
                }

                if(isset($_GET['id'])){
                    $id = $_GET['id'];
                    $equipo->setId($id);
                    
                    $save = $equipo->edit();
                }else{
                    $save = $equipo->save();
                }

                if($save){
                    $_SESSION['equipo'] = "complete";
                }else{
                    $_SESSION['equipo'] = "Failed";
                }
            }else{
                $_SESSION['equipo'] = "Failed";
            }
        }else{
            $_SESSION['equipo'] = "Failed";
        }
        header("Location:".base_url."proyecto/show&id=".$proyecto_id);
    }

    public function editar(){
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $edit = true;

            $equipo = new Equipo();
            $equipo->setId($id);
            
            $equi = $equipo->getOneByID();  

            require_once 'views/equipos/agregar.php';
        }else{
            header("Location:".base_url.'proyecto/show&proid=');
        }
    }

    public function eliminar(){
        Utilities::isAdmin();

        if(isset($_GET['id'])){
            $id = $_GET['id'];

            $equipo = new Equipo();
            $equipo->setId($id);

            $delete = $equipo->delete();

            if($delete){
                $_SESSION['delete'] = 'complete';
            }else{
                $_SESSION['delete'] = 'failed';
            }
        }else{
            $_SESSION['delete'] = 'failed';
        }
        $proid = $_GET['proid'];
        header("Location:".base_url."proyecto/show&id=".$proid);
    }

    public function show(){

        if(isset($_GET['id'])){
            $id = $_GET['id'];

            $equipo = new Equipo();
            $equipo->setId($id);

            $equi = $equipo->getOneByID();           
        }
        require_once 'views/equipos/show.php';
    }

} // FIN CLASE