<?php
require_once 'models/usuario.php';
require_once 'models/cliente.php';


class usuarioController{

    public function index(){
        echo "Controlador Usuarios, Acción index.";
    }

    public function registro(){
        require_once 'views/usuarios/registro.php';
    }

    public function gestionar(){
        $usuario = new Usuario();
        $usuarios = $usuario->getAll();

        require_once 'views/usuarios/gestionar.php';
    }

    public function ingreso(){
        require_once 'views/usuarios/ingreso.php';
    }

    public function save(){
        if(isset($_POST)){

            $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : false;
            $apellidos = isset($_POST['apellidos']) ? $_POST['apellidos'] : false;
            $email = isset($_POST['email']) ? $_POST['email'] : false;
            $password = isset($_POST['password']) ? $_POST['password'] : false;
            $rol = isset($_POST['rol']) ? $_POST['rol'] : false;

            if($nombre && $apellidos && $email && ($password || isset($_GET['id'])) && $rol){

                $usuario = new Usuario();
                $usuario->setNombre($nombre);
                $usuario->setApellidos($apellidos);
                $usuario->setEmail($email);
                $usuario->setPassword($password);
                $usuario->setRol($rol);

                if(isset($_GET['id'])){
                    $id = $_GET['id'];
                    
                    $usuario->setId($id);
                    $save = $usuario->edit();
                }else{
                    $save = $usuario->save();
                }

                if($save){
                    $_SESSION['register'] = "complete";
                }else{
                    $_SESSION['register'] = "failed";
                }
            }else{
                $_SESSION['register'] = "failed";
            }
        }else{
            $_SESSION['register'] = "failed";
        }
        header("Location:".base_url.'usuario/gestionar');
    }

    public function login(){
        //Asegurarse vienen datos del POST
        if(isset($_POST)){
            //if($_POST['rol'] == "admin"){
                $usuario = new Usuario();
                $usuario->setEmail($_POST['email']);
                $usuario->setPassword($_POST['password']);

                $identity = $usuario->login();
            //}else{
            //    $cliente = new Cliente();
            //    $cliente->setEmail($_POST['email']);
            //    $cliente->setPassword($_POST['password']);

            //    $identity = $cliente->login();
            //}
            //Crear una sesión
            if($identity && is_object($identity)){
                
                if($identity->rol == 'admin'){
                    $_SESSION['identity'] = $identity;
                    header("Location:".base_url."proyecto/gestionar");
                }else{
                    $_SESSION['identity'] = $identity;
                    header("Location:".base_url);
                }
            }else{
                $_SESSION['error_login'] = 'Identificacion Fallida';
                header("Location:".base_url);
            }

        }else{
            $_SESSION['error_login'] = 'Identificacion Fallida';
            header("Location:".base_url);
        }
        
    }

    public function logout(){
        if(isset($_SESSION['identity'])){
            unset($_SESSION['identity']);
        }
        if(isset($_SESSION['admin'])){
            unset($_SESSION['admin']);
        }
        header("Location:".base_url);
    }

    public function editar(){
        if(isset($_GET['id'])){
            
            $id = $_GET['id'];

            $edit = true;

            $usuarios = new Usuario();
            $usuarios->setId($id);
            
            $usuarios = $usuarios->getOneByID(); 
            
            require_once 'views/usuarios/registro.php';
        }else{
            header("Location:".base_url.'usuario/registro&id=');
        }

    }

    public function eliminar(){
        Utilities::isAdmin();

        if(isset($_GET['id'])){
            $id = $_GET['id'];

            $usuario = new Usuario();
            $usuario->setId($id);

            $delete = $usuario->delete();

            if($delete){
                $_SESSION['delete'] = 'complete';
            }else{
                $_SESSION['delete'] = 'failed';
            }
        }else{
            $_SESSION['delete'] = 'failed';
        }
        header("Location:".base_url.'usuario/gestionar');
    }

} // FIN CLASE