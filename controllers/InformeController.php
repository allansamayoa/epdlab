<?php
require_once 'models/informe.php';

class informeController{

    public function index(){
        echo "Controlador de INFORMES, Acción index.";
    }

    public function gestionar(){
        Utilities::isAdmin();
        
        $id = $_GET['id'];

        $informe = new Informe();
        $informe->setProyect_id($id);
        $informes = $informe->getSomeByProyectId();

        require_once 'views/informes/gestionar.php';
    }

    public static function informeByProyecto(){

        $id = $_GET['id'];

        $informe = new Informe();
        $informe->setProyect_id($id);
        $informes = $informe->getSomeByProyectId();
        /*var_dump($informes);
        die();
        */
        require_once 'views/informes/informeByProyecto.php';
    }

    public function crear(){
        Utilities::isAdmin();

        require_once 'views/informes/crear.php';
    }
    
    /*
    private $id;
        private $proyect_id;
        private $equipo_id;
        private $serie;
        private $fecha_informe;
        private $result_electricas;
        private $recom_electricas;
        private $result_aceite;
        private $recom_aceite;
        private $informe;
    */

    public function save(){
        Utilities::isAdmin();
        if(isset($_POST) && isset($_GET)){

            $proyect_id = isset($_GET['proid']) ? $_GET['proid'] : false;
            $equipo_id = isset($_POST['equipo']) ? $_POST['equipo'] : false;

            $fecha_informe = isset($_POST['fecha_informe']) ? $_POST['fecha_informe'] : false;
            $result_electricas = isset($_POST['result_electricas']) ? $_POST['result_electricas'] : false;
            $recom_electricas = isset($_POST['recom_electricas']) ? $_POST['recom_electricas'] : false;
            $result_aceite = isset($_POST['result_aceite']) ? $_POST['result_aceite'] : false;
            $recom_aceite = isset($_POST['recom_aceite']) ? $_POST['recom_aceite'] : false;
            // $informe = isset($_POST['informe']) ? $_POST['informe'] : false;

            if($proyect_id && $equipo_id && $fecha_informe && ($result_electricas || $result_aceite) && ($recom_electricas  || $recom_aceite) ){
                if (empty($recom_electricas)) {
                    $result_electricas='';
                }
                if (empty($recom_aceite)) {
                    $result_aceite='';
                }
                $admin_id = $_SESSION['identity']->id;
                
                $recom_electricas = empty($recom_electricas) || $recom_electricas==''? 'N/A' : $recom_electricas;
                $recom_aceite = empty($recom_aceite) || $recom_aceite==''? 'N/A' : $recom_aceite;

                $informe = new Informe();
                $informe->setProyect_id($proyect_id);
                $informe->setEquipo_id($equipo_id);

                    $equi = Utilities::showSelectedEquipo($equipo_id);
                    $serie_id = $equi->serie;
                    
                $informe->setSerie($serie_id);
    
                $informe->setFecha_informe($fecha_informe);
                $informe->setResult_electricas($result_electricas);
                $informe->setRecom_electricas($recom_electricas);
                $informe->setResult_aceite($result_aceite);
                $informe->setRecom_aceite($recom_aceite);

                //Guardar la Imagen
                if(isset($_FILES['archivo'])){
                    $archivo = $_FILES['archivo'];
                    $filename = $archivo['name'];
                    $mimetype = $archivo['type'];

                    if($mimetype == "application/pdf"  || $mimetype == "image/jpeg" || $mimetype == "image/png" || $mimetype == "image/gif"){
                        
                        if(!is_dir('uploads/informes')){
                            mkdir('uploads/informes', 0777, true);
                        }

                        $informe->setInforme($filename);
                        move_uploaded_file($archivo['tmp_name'], 'uploads/informes/'.$filename);
                    }
                }

                if(isset($_FILES['archivo_pa'])){
                    $archivo = $_FILES['archivo_pa'];
                    $filename = $archivo['name'];
                    $mimetype = $archivo['type'];

                    if($mimetype == "application/pdf"  || $mimetype == "image/jpeg" || $mimetype == "image/png" || $mimetype == "image/gif"){
                        
                        if(!is_dir('uploads/informes')){
                            mkdir('uploads/informes', 0777, true);
                        }

                        $informe->setInforme_pa($filename);
                        move_uploaded_file($archivo['tmp_name'], 'uploads/informes/'.$filename);
                    }
                }
                if(isset($_GET['id'])){
                    $id = $_GET['id'];

                    /*var_dump($id);
                    echo "Estoy en EDIT";
                    die();*/


                    $informe->setId($id);

                    $save = $informe->edit();
                }else{
                    
                    /*var_dump($_GET['id']);
                    echo "Estoy en SAVE";
                    die();*/

                    $save = $informe->save();
                }

                if($save){
                    $_SESSION['informe'] = "complete";
                }else{
                    $_SESSION['informe'] = "Failed";
                }
            }else{
                $_SESSION['informe'] = "Failed";
            }
        }else{
            $_SESSION['informe'] = "Failed";
        }
        header("Location:".base_url."informe/informeByProyecto&id=".$proyect_id);
    }

    public function eliminar(){
        Utilities::isAdmin();

        if(isset($_GET['id'])){
            $id = $_GET['id'];

            $informe = new Informe();
            $informe->setId($id);

            $delete = $informe->delete();

            if($delete){
                $_SESSION['delete'] = 'complete';
            }else{
                $_SESSION['delete'] = 'failed';
            }
        }else{
            $_SESSION['delete'] = 'failed';
        }
        $proid = $_GET['proid'];
        $equid = $_GET['equid'];
        header("Location:".base_url."proyecto/show&id=".$proid);
    }

    public function editar(){
        if(isset($_GET['id'])){
            $id = $_GET['id'];
            $edit = true;

            $informe = new Informe();
            $informe->setId($id);
            
            $informe = $informe->getOneByID(); 
            
            require_once 'views/informes/editar.php';
        }else{
            header("Location:".base_url.'proyecto/show&proid=');
        }

    }


} // FIN CLASE