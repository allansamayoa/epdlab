<?php

class Informe{
	private $id;
	private $proyect_id;
	private $equipo_id;
	private $serie;
	private $fecha_informe;
	private $result_electricas;
	private $recom_electricas;
	private $result_aceite;
	private $recom_aceite;
	private $informe;
	private $informe_pa;

	private $db;
   
	//CONSTRUCTOR

	public function __construct() {
		$this->db = Database::connect();
	}


	//GETTERS

	public function getId(){
		return $this->id;
	}

	public function getProyect_id(){
		return $this->proyect_id;
	}

	public function getEquipo_id(){
		return $this->equipo_id;
	}

	public function getSerie(){
		return $this->serie;
	}

	public function getFecha_informe(){
		return $this->fecha_informe;
	}

	public function getResult_electricas(){
		return $this->result_electricas;
	}

	public function getRecom_electricas(){
		return $this->recom_electricas;
	}

	public function getResult_aceite(){
		return $this->result_aceite;
	}

	public function getRecom_aceite(){
		return $this->recom_aceite;
	}

	public function getInforme(){
		return $this->informe;
	}

	public function getInforme_pa(){
		return $this->informe_pa;
	}
  
	// SETTERS

	
	public function setId($id){
		$this->id = $id;
	}

	public function setProyect_id($proyect_id){
		$this->proyect_id = $proyect_id;
	}

	public function setEquipo_id($equipo_id){
		$this->equipo_id = $equipo_id;
	}

	public function setSerie($serie){
		$this->serie = $serie;
	}

	public function setFecha_informe($fecha_informe){
		$this->fecha_informe = $fecha_informe;
	}

	public function setResult_electricas($result_electricas){
		$this->result_electricas = $result_electricas;
	}

	public function setRecom_electricas($recom_electricas){
		$this->recom_electricas = $recom_electricas;
	}

	public function setResult_aceite($result_aceite){
		$this->result_aceite = $result_aceite;
	}

	public function setRecom_aceite($recom_aceite){
		$this->recom_aceite = $recom_aceite;
	}

	public function setInforme($informe){
		$this->informe = $informe;
	}

	public function setInforme_pa($informe_pa){
		$this->informe_pa = $informe_pa;
	}
  
	// OTROS MÉTODOS - ACCIONES

	public function getAll(){
		$sql = "SELECT id,proyect_id,equipo_id,serie,DATE_FORMAT(fecha_informe,'%Y-%b-%d') as fecha_informe,result_electricas,recom_electricas,result_aceite,recom_aceite,archivo,archivo_pa FROM informes ORDER BY id DESC;";
		$informes = $this->db->query($sql);

		return $informes;       
	}

	public function getStatus(){
		$sql = "SELECT
				IFNULL(
					(
						SELECT
							p.resultado
						FROM
							equipos AS e
						INNER JOIN pruebas AS p ON p.equipo_id = e.id
						WHERE
							e.proyecto_id = '{$this->proyect_id}'
						AND p.tipo_prueba = 1
						AND e.id = '{$this->equipo_id}'
						ORDER BY
							p.resultado ASC
						LIMIT 1
					),
					0
				) AS 'electrica',
				IFNULL(
					(
						SELECT
							p.resultado
						FROM
							equipos AS e
						INNER JOIN pruebas AS p ON p.equipo_id = e.id
						WHERE
							e.proyecto_id = '{$this->proyect_id}'
						AND p.tipo_prueba = 2
						AND e.id = '{$this->equipo_id}'
						ORDER BY
							p.resultado ASC
						LIMIT 1
					),
					0
				) AS 'aceite'
			";
		$informes = $this->db->query($sql);

		return $informes;       
	}
	
	public function getSomeByProyectId(){
		$sql = "SELECT id,proyect_id,equipo_id,serie,DATE_FORMAT(fecha_informe,'%Y-%b-%d') as fecha_informe,result_electricas,recom_electricas,result_aceite,recom_aceite,archivo,archivo_pa FROM informes WHERE proyect_id = '{$this->proyect_id}' ORDER BY id DESC;";
		$informes = $this->db->query($sql);
		return $informes;
	}

	/*
	private $id;
	private $proyect_id;
	private $equipo_id;
	private $serie;
	private $fecha_informe;
	private $result_electricas;
	private $recom_electricas;
	private $result_aceite;
	private $recom_aceite;
	private $informe;
	*/
	public function save(){
		$sql = "INSERT INTO informes
				(
					proyect_id, equipo_id, serie, fecha_informe, result_electricas, recom_electricas, result_aceite, recom_aceite, archivo, archivo_pa
				) 
				VALUES ('{$this->getProyect_id()}', '{$this->getEquipo_id()}', '{$this->getSerie()}', '{$this->getFecha_informe()}', '{$this->getResult_electricas()}', '{$this->getRecom_electricas()}', '{$this->getResult_aceite()}', '{$this->getRecom_aceite()}', '{$this->getInforme()}', '{$this->getInforme_pa()}');";
		$save = $this->db->query($sql);

		$result = false;
		if($save){
			$result = true;
		}
		return $result;
	}

	public function edit(){

		/*var_dump($this->getId());
		echo "Estoy en el MODELO FUNC EDIT";
		die();*/

		$sql = "UPDATE informes SET equipo_id='{$this->getEquipo_id()}', serie='{$this->getSerie()}', fecha_informe='{$this->getFecha_informe()}', result_electricas='{$this->getResult_electricas()}', recom_electricas='{$this->getRecom_electricas()}', result_aceite='{$this->getResult_aceite()}', recom_aceite='{$this->getRecom_aceite()}' ";
		
		if($this->getInforme() != null){
			$sql .= ", archivo='{$this->getInforme()}' ";
		}else{
			$sql .= " ";
		}

		if($this->getInforme_pa() != null){
			$sql .= ", archivo_pa='{$this->getInforme_pa()}' ";
		}else{
			$sql .= " ";
		}

		$sql .= " WHERE id={$this->getId()};";
		
		
		$save = $this->db->query($sql);

		$result = false;
		if($save){
			$result = true;
		}
		return $result;
	}

	public function delete(){
		$sql = "DELETE FROM informes WHERE id={$this->id}";
		$delete = $this->db->query($sql);
	
		$result = false;
			if($delete){
				$result = true;
			}
			return $result;
		}

	public function getOneById(){
		$sql = "SELECT id,proyect_id,equipo_id,serie,DATE_FORMAT(fecha_informe,'%Y-%m-%d') as fecha_informe,result_electricas,recom_electricas,result_aceite,recom_aceite,archivo,archivo_pa FROM informes WHERE id = '{$this->getid()}';";
		$informe = $this->db->query($sql);
		return $informe->fetch_object();
	}
}//FIN DE CLASE
	



