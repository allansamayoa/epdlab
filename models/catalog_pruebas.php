<?php

class catalog_prueba{
    private $id;
    private $nombre;
    private $abreviacion;
    private $tipo_prueba;
    private $fecha_crea;

    private $db;
   

    //CONSTRUCTOR

    public function __construct() {
        $this->db = Database::connect();
    }


    //GETTERS

    public function getId(){
        return $this->id;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getAbreviacion(){
        return $this->abreviacion;
    }

    public function getTipo_prueba(){
        return $this->tipo_prueba;
    }

    public function getFecha_prueba(){
        return $this->fecha_crea;
    }
  
    // SETTERS
   
    public function setId($id){
        $this->id = $id;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function setAbreviacion($abreviacion){
        $this->abreviacion = $abreviacion;
    }

    public function setTipo_prueba($tipo_prueba){
        $this->tipo_prueba = $tipo_prueba;
    }

    public function setFecha_prueba($fecha_prueba){
        $this->fecha_prueba = $fecha_crea;
    }

    // OTROS MÉTODOS - ACCIONES

    public function getAll(){
        $sql = "SELECT id, nombre, abreviacion, tipo_prueba, DATE_FORMAT(fecha_crea,'%Y-%b-%d') as fecha_crea FROM catalog_pruebas ORDER BY id DESC;";
        $cat_pruebas = $this->db->query($sql);

        return $cat_pruebas;
    }

    public function getOneById(){
        $sql = "SELECT id, nombre, abreviacion, tipo_prueba, DATE_FORMAT(fecha_crea,'%Y-%b-%d') as fecha_crea FROM catalog_pruebas WHERE id = '{$this->getid()}';";
        $cat_prueba = $this->db->query($sql);
        return $cat_prueba->fetch_object();
    }

    public function getByType(){
        $sql = "SELECT id, nombre, abreviacion, tipo_prueba, DATE_FORMAT(fecha_crea,'%Y-%b-%d') as fecha_crea FROM catalog_pruebas WHERE tipo_prueba = '{$this->getTipo_prueba()}';";
        $cat_pruebas = $this->db->query($sql);
        return $cat_pruebas;
    }

    /*  private $id;
            private $equipo_id;
        private $nombre;
        private $abreviacion;
        private $tipo_prueba;
        private $resultado;
        private $fecha_prueba;
        private $archivo;
    */

    public function save(){
        $sql = "INSERT INTO catalog_pruebas VALUES (null, '{$this->getNombre()}', '{$this->getAbreviacion()}', '{$this->getTipo_prueba()}', '{$this->getFecha_crea()}');";
        $save = $this->db->query($sql);

        $result = false;
        if($save){
            $result = true;
        }
        return $result;
    }

    public function delete(){
        $sql = "DELETE FROM catalog_pruebas WHERE id={$this->id}";
        $delete = $this->db->query($sql);
    
        $result = false;
            if($delete){
                $result = true;
            }
            return $result;
        }

    public function edit(){

        $sql = "UPDATE catalog_pruebas SET nombre='{$this->getNombre()}', fecha_crea='{$this->getFecha_crea()}' WHERE id={$this->getId()}";       
        $save = $this->db->query($sql);

        $result = false;
        if($save){
            $result = true;
        }
        return $result;
    }
   
}//FIN DE CLASE
    



