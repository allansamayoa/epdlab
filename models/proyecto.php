<?php

class Proyecto{
    private $id;
    private $admin_id;
    private $cliente_id;
    private $nombre;
    private $ciudad;
    private $descripcion;
    private $fecha_crea;

    private $db;
   
    //CONSTRUCTOR

    public function __construct() {
        $this->db = Database::connect();
    }

    //GETTERS

    public function getId(){
        return $this->id;
    }

    public function getAdmin_id(){
        return $this->admin_id;
    }

    public function getCliente_id(){
        return $this->cliente_id;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getCiudad(){
        return $this->ciudad;
    }

    public function getDescripcion(){
        return $this->descripcion;
    }

    public function getFecha_crea(){
        return $this->fecha_crea;
    }
    
    // SETTERS

    public function setId($id){
        $this->id = $id;
    }

    public function setAdmin_id($admin_id){
        $this->admin_id = $admin_id;
    }

    public function setCliente_id($cliente_id){
        $this->cliente_id = $cliente_id;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function setCiudad($ciudad){
        $this->ciudad = $ciudad;
    }

    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }

    public function setFecha_crea($fecha_crea){
        $this->fecha_crea = $fecha_crea;
    }
    
    // ***************************    OTROS MÉTODOS - ACCIONES **************************************** // 

    public function getAll(){
        $sql = "SELECT id,admin_id,cliente_id,nombre,ciudad,descripcion,DATE_FORMAT(fecha_crea,'%Y-%b-%d') AS fecha_crea FROM proyectos ORDER BY id DESC;";
        $proyectos = $this->db->query($sql);
        return $proyectos;
    }

    public function getSomeByAdminId(){
        $sql = "SELECT id,admin_id,cliente_id,nombre,ciudad,descripcion,DATE_FORMAT(fecha_crea,'%Y-%b-%d') AS fecha_crea FROM proyectos WHERE admin_id = '{$this->getAdmin_id()}' ORDER BY id DESC;";
        $proyectos = $this->db->query($sql);
        return $proyectos;
    }

    public function getSomeByClienteId(){
        $sql = "SELECT id,admin_id,cliente_id,nombre,ciudad,descripcion,DATE_FORMAT(fecha_crea,'%Y-%b-%d') AS fecha_crea FROM proyectos WHERE cliente_id = '{$this->getCliente_id()}' ORDER BY id DESC;";
        $proyectos = $this->db->query($sql);
        return $proyectos;
    }

    public function save(){
        $sql = "INSERT INTO proyectos VALUES (null,'{$this->getAdmin_id()}', '{$this->getCliente_id()}', '{$this->getNombre()}', '{$this->getCiudad()}', '{$this->getDescripcion()}', CURDATE());";
        $save = $this->db->query($sql);

        $result = false;
        if($save){
            $result = true;
        }
        return $result;
    }

    public function edit(){
        $sql = "UPDATE proyectos SET cliente_id='{$this->getCliente_id()}', nombre='{$this->getNombre()}', ciudad='{$this->getCiudad()}', descripcion='{$this->getDescripcion()}' WHERE id='{$this->getId()}';";
        $save = $this->db->query($sql);

        $result = false;
        if($save){
            $result = true;
        }
        return $result;
    }

    public function getOneById(){
        $sql = "SELECT id,admin_id,cliente_id,nombre,ciudad,descripcion,DATE_FORMAT(fecha_crea,'%Y-%b-%d') AS fecha_crea FROM proyectos WHERE id = '{$this->getId()}';";
        $proyecto = $this->db->query($sql);
        return $proyecto->fetch_object();
    }

    public function delete(){
        $sql = "DELETE FROM proyectos WHERE id ={$this->id}";
        $delete = $this->db->query($sql);
    
        $result = false;
            if($delete){
                $result = true;
            }
            return $result;
        }

}
    