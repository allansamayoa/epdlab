<?php

class Cliente{
    private $id;
    private $admin_id;
    private $nombre;
    private $apellidos;
    private $email;
    private $password;
    private $rol;
    private $empresa;
    private $ciudad;
    private $vendedor;
    private $fecha_reg;
    
    private $db;
   
    //CONSTRUCTOR

    public function __construct() {
        $this->db = Database::connect();
    }

    //GETTERS

    

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of admin_id
     */ 
    public function getAdmin_id()
    {
        return $this->admin_id;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of apellidos
     */ 
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return password_hash($this->db->real_escape_string($this->password), PASSWORD_BCRYPT, ['cost' => 4]);
    }

    /**
     * Get the value of email
     */ 
    public function getRol()
    {
        return $this->rol;
    }


    /**
     * Get the value of empresa
     */ 
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Get the value of ciudad
     */ 
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Get the value of vendedor
     */ 
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * Get the value of fecha_reg
     */ 
    public function getFecha_reg()
    {
        return $this->fecha_reg;
    }

    // SETTERS

    public function setId($id){
        $this->id = $id;
    }

    public function setAdmin_id($admin_id){
        $this->admin_id = $admin_id;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function setApellidos($apellidos){
        $this->apellidos = $apellidos;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function setPassword($password){
        $this->password = $password;
    }

    public function setRol($rol){
        $this->rol = $rol;
    }

    public function setEmpresa($empresa){
        $this->empresa = $empresa;
    }

    public function setCiudad($ciudad){
        $this->ciudad = $ciudad;
    }

    public function setVendedor($vendedor){
        $this->vendedor = $vendedor;
    }

    public function setFecha_reg($fecha_reg){
        $this->fecha_reg = $fecha_reg;
    }

    // OTROS MÉTODOS - ACCIONES

    public function getAll(){
        $sql = "SELECT id, admin_id, nombre, apellidos, email, `password`, rol, empresa, ciudad, vendedor, DATE_FORMAT(fecha_reg,'%Y-%b-%d') AS fecha_reg 
                FROM usuarios WHERE rol='cliente';";
        $clientes = $this->db->query($sql);
        return $clientes;
    }

    
    public function save(){
        $sql = "INSERT INTO usuarios VALUES (null,'{$this->getAdmin_id()}', '{$this->getNombre()}', '{$this->getApellidos()}', '{$this->getEmail()}', '{$this->getPassword()}', '{$this->getRol()}', '{$this->getEmpresa()}', '{$this->getCiudad()}', '{$this->getVendedor()}', CURDATE());";

        $save = $this->db->query($sql);

        $result = false;
        if($save){
            $result = true;
        }
        return $result;
    }

    public function getOneById(){
        $sql = "SELECT id, admin_id, nombre, apellidos, email, `password`, rol, empresa, ciudad, vendedor, DATE_FORMAT(fecha_reg,'%Y-%b-%d') AS fecha_reg 
        FROM usuarios WHERE id = '{$this->getid()}';";
        $equipo = $this->db->query($sql);
        return $equipo->fetch_object();
    }

    public function login(){
        $result = false;
        $email = $this->email;
        $password = $this->password;

        //Comprobar si existe el Cliente
        $sql = "SELECT * FROM usuarios WHERE email = '$email'";
        $login = $this->db->query($sql);

        if($login && $login->num_rows == 1){
            $cliente = $login->fetch_object();

            //Verificar la contraseña
            $verify = password_verify($password, $cliente->password);

            if($verify){
                $result = $cliente;
            }
        }
        return $result;
    }

    public function delete(){
        $sql = "DELETE FROM usuarios WHERE id ={$this->id}";
        $delete = $this->db->query($sql);
    
        $result = false;
            if($delete){
                $result = true;
            }
            return $result;
        }

        public function edit(){
            $sql = "UPDATE usuarios SET nombre='{$this->getNombre()}', apellidos='{$this->getApellidos()}', `password` ='{$this->getPassword()}', email='{$this->getEmail()}', empresa='{$this->getEmpresa()}', ciudad='{$this->getCiudad()}' WHERE id='{$this->getId()}' ;";
            //$sql = "UPDATE clientes SET `password` ='{$this->getPassword()}' WHERE id='{$this->getId()}' ;";
    
            $save = $this->db->query($sql);
    
            $result = false;
            if($save){
                $result = true;
            }
            return $result;
        }
}
    