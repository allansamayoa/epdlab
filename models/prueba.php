<?php

class Prueba{
    private $id;
    private $equipo_id;
    private $nombre;
    private $abreviacion;
    private $tipo_prueba;
    private $resultado;
    private $recomend;
    private $fecha_prueba;
    private $archivo;

    private $db;
   

    //CONSTRUCTOR

    public function __construct() {
        $this->db = Database::connect();
    }


    //GETTERS

    public function getId(){
        return $this->id;
    }

    public function getEquipo_id(){
        return $this->equipo_id;
    }
 
    public function getNombre(){
        return $this->nombre;
    }

    public function getAbreviacion(){
        return $this->abreviacion;
    }

    public function getTipo_prueba(){
        return $this->tipo_prueba;
    }

    public function getResultado(){
        return $this->resultado;
    }

    public function getRecomend(){
        return $this->recomend;
    }

    public function getFecha_prueba(){
        return $this->fecha_prueba;
    }

    public function getArchivo(){
        return $this->archivo;
    }

        
    // SETTERS
   
    public function setId($id){
        $this->id = $id;
    }

    public function setEquipo_id($equipo_id){
        $this->equipo_id = $equipo_id;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function setAbreviacion($abreviacion){
        $this->abreviacion = $abreviacion;
    }

    public function setTipo_prueba($tipo_prueba){
        $this->tipo_prueba = $tipo_prueba;
    }

    public function setResultado($resultado){
        $this->resultado = $resultado;
    }

    public function setRecomend($recomend){
        $this->recomend = $recomend;
    }

    public function setFecha_prueba($fecha_prueba){
        $this->fecha_prueba = $fecha_prueba;
    }

    public function setArchivo($archivo){
        $this->archivo = $archivo;
    }

  
    // OTROS MÉTODOS - ACCIONES

    public function getAll(){
        $sql = "SELECT id, equipo_id, nombre, abreviacion, tipo_prueba, resultado, recomend, DATE_FORMAT(fecha_prueba,'%Y-%b-%d') AS fecha_prueba, archivo FROM pruebas ORDER BY id DESC;";
        $pruebas = $this->db->query($sql);

        return $pruebas;
    }

    public function getOneById(){
        $sql = "SELECT id, equipo_id, nombre, abreviacion, tipo_prueba, resultado, recomend, DATE_FORMAT(fecha_prueba,'%Y-%m-%d') AS fecha_prueba, archivo FROM pruebas WHERE id = '{$this->getid()}';";
        $prueba = $this->db->query($sql);
        return $prueba->fetch_object();
    }

    /*  private $id;
            private $equipo_id;
        private $nombre;
        private $abreviacion;
        private $tipo_prueba;
        private $resultado;
        private $fecha_prueba;
        private $archivo;
    */

    public function save(){
        $sql = "INSERT INTO pruebas VALUES (null,'{$this->getEquipo_id()}', '{$this->getNombre()}', '{$this->getAbreviacion()}', '{$this->getTipo_prueba()}', '{$this->getResultado()}', '{$this->getRecomend()}', '{$this->getFecha_prueba()}', '{$this->getArchivo()}');";
        $save = $this->db->query($sql);

        $result = false;
        if($save){
            $result = true;
        }
        return $result;
    }

    public function getSomeByEquipoId(){
        $sql = "SELECT id, equipo_id, nombre, abreviacion, tipo_prueba, resultado, recomend, DATE_FORMAT(fecha_prueba,'%Y-%b-%d') AS fecha_prueba, archivo FROM pruebas WHERE equipo_id = '{$this->getEquipo_id()}' ORDER BY id DESC;";
        $equipos = $this->db->query($sql);
        return $equipos;
    }

    public function delete(){
        $sql = "DELETE FROM pruebas WHERE id={$this->id}";
        $delete = $this->db->query($sql);
    
        $result = false;
            if($delete){
                $result = true;
            }
            return $result;
        }

    public function edit(){

        $sql = "UPDATE pruebas SET nombre='{$this->getNombre()}', resultado='{$this->getResultado()}', recomend='{$this->getRecomend()}', fecha_prueba='{$this->getFecha_prueba()}' ";       

        if($this->getArchivo() != null){
            $sql .= ", archivo='{$this->getArchivo()}' ";
        }else{
            $sql .= " ";
        }

        $sql .= " WHERE id={$this->getId()};";

        $save = $this->db->query($sql);

        $result = false;
        if($save){
            $result = true;
        }
        return $result;
    }
   
}//FIN DE CLASE
    



