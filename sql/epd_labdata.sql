-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-06-2019 a las 01:50:08
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `epd_labdata`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) NOT NULL,
  `admin_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol` varchar(100) NOT NULL,
  `empresa` varchar(100) DEFAULT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `vendedor` varchar(100) DEFAULT NULL,
  `fecha_reg` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `admin_id`, `nombre`, `apellidos`, `email`, `password`, `rol`, `empresa`, `ciudad`, `vendedor`, `fecha_reg`) VALUES
(4, 1, 'Cliente', 'Ejemplo', 'cliente@cliente.com', '$2y$04$Jb.ZI5qTTbkLiDkvvXYg/OrqNZRq8JLE8i7bPSJsAARphojHtpmMy', 'cliente', 'EMPRESA MODELO', 'SAN PEDRO SULA', 'Carmen Boquin', '2019-06-14'),
(7, 4, 'Pedro', 'Abarca', 'pedro@pedro.com', '$2y$04$UNyDHdKtf2vJkhLBYzPjeuV9VuCTgnq2i83rqD.rFfBtP3uNr.bSa', 'cliente', 'PETER CORP', 'San Pedro Sula', 'Claudia Martinez', '2019-06-29'),
(9, 1, 'Allan ', 'Samayoa', 'asamayoa@pixelmediahn.com', '$2y$04$wuMHpeOJ3DW1MKmhRH2FeOe0h/t7tqdstA.Ew72rb.MQbNyzW4B5e', 'cliente', 'PIXEL MEDIA', 'SAN PEDRO SULA', 'Claudia Martinez', '2019-06-29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(255) NOT NULL,
  `proyecto_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `marca` varchar(100) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `serie` varchar(100) NOT NULL,
  `fabricante` varchar(100) NOT NULL,
  `descripcion` text,
  `fecha_crea` date NOT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `proyecto_id`, `nombre`, `marca`, `modelo`, `serie`, `fabricante`, `descripcion`, `fecha_crea`, `imagen`) VALUES
(56, 14, 'EQUIPO MARIANT', 'SHELL', 'EQUIMA', '85874EQ', 'SHELL', 'Lorem ipsun dolor sit amet donec', '2019-06-27', 'BAGU4157 (1).JPG'),
(57, 14, 'EQUIPO TESTO', 'ROLAND', 'TERT', 'RTYY8822', 'ROLAND', 'More than we can imagine', '2019-06-27', 'FBRZ2073 (1).JPG'),
(58, 27, 'EQUIPO DE TESTEO', 'TESTA MOTOR CO.', 'TE99', 'TEQ-254781', 'TESTA MOTOR CO.', 'Equipo de prueba para ensayos de la herramienta.', '2019-06-27', '63000-3ranges-1.jpg'),
(59, 27, 'PLANTA ELECTRICA', 'SHERWING WILLIAMS', 'TP8000', 'TPL-857412', 'SHERWING WILLIAMS', 'Una planta interesante para la aplicacion de novedades', '2019-06-27', 'planta-energia-rdrental-02.jpg'),
(60, 28, 'DRON MAVERIC', 'TESTLA', 'MD-8765', '542100', 'TESTLA', 'Lorem Ipsun dolro sit amet', '2019-06-27', 'electroimanes-accionamiento.jpg'),
(61, 28, 'TELEFONO PLANTADOS', 'BELL COMPUTERS', 'BELL', 'BELL-857412', 'BELL COMPUTERS', 'El telefono especial para todo tipo de llamadsa', '2019-06-27', 'telefono-antiguo.jpg'),
(62, 28, 'BOMBA AGUA AZUL', 'BOMOSA S:A:', 'BOMOBLUE', 'BM857412', 'BOMOSA S:A:', 'La bomba de agua más potente de la región', '2019-06-27', '380.jpg'),
(63, 29, 'TRANSFORMADOR', 'EEH COLOMBIA', 'TR2000', 'TRAFO-857412', 'EEH COLOMBIA', 'Un transformador espectacular con la capacidad de 25kw por hora de procesamiento.', '2019-06-27', '20368787-transformador-viejo-en-poste-eléctrico.jpg'),
(64, 29, 'SELECCIONADORA', 'AMBLIM ENT.', 'METALGEAR', 'MG-857412', 'AMBLIM ENT.', 'Esta seleccionadora sirve para seleccionar todo lo seleccionable en una seleccion natural.', '2019-06-27', 'Selezionatrice-per-uova-S91.jpg'),
(65, 28, 'COSA', 'jjashd', 'CASDA', 'asds', 'jjashd', 'jsdghaoslhdphqdf', '2019-06-28', 'Selezionatrice-per-uova-S91.jpg'),
(66, 30, 'TRANSFORMADOR', 'WESTINGHOUSE', 'T-857', 'TRF-547893', 'WESTINGHOUSE', 'Este transformador tiene la capacidad de procesar 25kw al día', '2019-06-29', '20368787-transformador-viejo-en-poste-eléctrico.jpg'),
(67, 30, 'SELECCIONADORA', 'TORNOS MANUEL', 'SC7874', '874568578898', 'TORNOS MANUEL', 'Puede procesar 50 kms de material por hora.', '2019-06-29', 'Selezionatrice-per-uova-S91.jpg'),
(68, 30, 'SINCRONIZADOR', 'SONY CORP', 'ST987', '665464656354HDFG74654', 'SONY INC.', 'Capaz de tomar frecuencias de todas las estaciones radiales del mundo', '2019-06-29', 'electroimanes-accionamiento.jpg'),
(69, 31, 'COMPUITADORA', 'DELL', 'STYLUS', 'D857412', 'DELL', 'Para edicion sobre todo y diseño gráfico', '2019-06-29', 'd75631b3-f9cf-4054-8e41-acbae306180f_1.a8cb629e99e3401007557845045e5768_1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informes`
--

CREATE TABLE `informes` (
  `id` int(255) NOT NULL,
  `proyect_id` int(255) NOT NULL,
  `equipo_id` int(255) NOT NULL,
  `serie` varchar(255) NOT NULL,
  `fecha_informe` date NOT NULL,
  `result_electricas` int(3) NOT NULL,
  `recom_electricas` varchar(255) DEFAULT NULL,
  `result_aceite` int(3) NOT NULL,
  `recom_aceite` varchar(255) DEFAULT NULL,
  `archivo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `informes`
--

INSERT INTO `informes` (`id`, `proyect_id`, `equipo_id`, `serie`, `fecha_informe`, `result_electricas`, `recom_electricas`, `result_aceite`, `recom_aceite`, `archivo`) VALUES
(21, 14, 57, 'RTYY8822', '2019-06-26', 3, '                Ingrese sus recomendaciones\r\n                ', 1, '                Ingrese sus recomendaciones\r\n                ', '03. estimacion-presupuesto-rmedia-softech.pdf'),
(22, 14, 56, '85874EQ', '2019-06-26', 1, '                Ingrese sus recomendaciones\r\n                ', 2, '                Ingrese sus recomendaciones\r\n                ', 'cap210 (1).pdf'),
(23, 14, 57, 'RTYY8822', '2019-06-27', 1, '                Ingrese sus recomendaciones\r\n                ', 1, '                Ingrese sus recomendaciones\r\n                ', 'once_noticias_presupuesto-softech.pdf'),
(24, 27, 59, 'TPL-857412', '2019-06-26', 2, '                Ingrese sus recomendaciones\r\n                ', 1, '                Ingrese sus recomendaciones\r\n                ', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf'),
(36, 29, 64, 'MG-857412', '2019-06-28', 3, 'asdfasdfasfa', 1, 'asdfasdfasdf', 'cap210 (1).pdf'),
(37, 29, 63, 'TRAFO-857412', '2019-06-23', 2, 'fasdggdhfg', 3, 'safdhgkgasd', 'once_noticias_presupuesto-softech.pdf'),
(49, 28, 61, 'BELL-857412', '2019-06-27', 3, 'Estamos en un estado ACEPTABLE', 2, 'Estamos en un estsado CUESTIONABLE', 'cap210 (1).pdf'),
(50, 30, 68, '665464656354HDFG74654', '2019-06-29', 1, 'Revisar una prueba crítica', 3, 'N/A', '03. estimacion-presupuesto-rmedia-softech.pdf'),
(51, 31, 69, 'D857412', '2019-06-29', 3, 'En excelentes condiciones', 2, 'Cuestionable algunos puntos', 'once_noticias_presupuesto-softech.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `id` int(255) NOT NULL,
  `admin_id` int(255) NOT NULL,
  `cliente_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `fecha_crea` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`id`, `admin_id`, `cliente_id`, `nombre`, `ciudad`, `descripcion`, `fecha_crea`) VALUES
(14, 3, 4, 'PRUEBA MARIAN', 'SAP', 'Estos son los datos relevantes sobre el proyecto', '2019-06-20'),
(27, 1, 4, 'TEST PRO', 'Cuyamel', 'Lorem ipsun dolor sit amet.', '2019-06-27'),
(28, 1, 4, 'OPC  PUERTO', 'Puerto Cortés, Cortés', 'La instalacion de una aduana general', '2019-06-27'),
(29, 3, 4, 'ELCATEX', 'Cholom', 'Proceso de grabacion de video', '2019-06-27'),
(30, 4, 7, 'PLANTA DE TRATAMIENTO', 'Choloma, Cortés', 'Esta planta pretende hacer el tratamiento mensual de 27 toneladas de agua', '2019-06-29'),
(31, 1, 9, 'EDIFICIO PIXEL MEDIA', 'SAN PEDRO SULA', 'Los estudios de TV mas grandes de la región', '2019-06-29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pruebas`
--

CREATE TABLE `pruebas` (
  `id` int(255) NOT NULL,
  `equipo_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `abreviacion` varchar(100) NOT NULL,
  `tipo_prueba` int(2) NOT NULL,
  `resultado` int(3) NOT NULL,
  `recomend` varchar(255) NOT NULL,
  `fecha_prueba` date NOT NULL,
  `archivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pruebas`
--

INSERT INTO `pruebas` (`id`, `equipo_id`, `nombre`, `abreviacion`, `tipo_prueba`, `resultado`, `recomend`, `fecha_prueba`, `archivo`) VALUES
(43, 59, 'MUY BAJA FRECUENCIA', 'ABC', 1, 2, '            De acuerdo a los resultados se recomienda: \r\n            ', '2019-06-27', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf'),
(44, 59, 'COMPUESTOS FURANOS', 'ABC', 2, 3, '            De acuerdo a los resultados se recomienda: \r\n            ', '2019-06-27', 'once_noticias_presupuesto-softech.pdf'),
(45, 58, 'TIP UP', 'ABC', 1, 2, '            De acuerdo a los resultados se recomienda: \r\n            ', '2019-06-27', 'cap210 (1).pdf'),
(47, 62, 'COLO E INSPECCION VISUAL', 'ABC', 2, 2, 'Esta todo tranquilo pero cuestionable', '2019-06-26', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf'),
(48, 62, 'EXAFLORURO DE AZUFRE', 'ABC', 1, 3, 'Muy bien todo por este departamento', '2019-06-26', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf'),
(49, 57, 'POLRIDAD', 'ABC', 1, 1, 'La polaridad de estas 2 fuerzas es diametralmente opuesta. Mantenganse alejados.', '2019-06-21', 'cap210 (1).pdf'),
(50, 56, 'INDICE DE ACIDEZ', 'ABC', 2, 3, 'Todos son unos ÁCIDOS por lo que andan super bien', '2019-06-26', 'cap210 (1).pdf'),
(51, 64, 'MUY BAJA FRECUENCIA', 'ABC', 1, 3, 'asdasdasd', '2019-06-26', ''),
(52, 64, 'TERMOGRAFIA', 'ABC', 1, 3, 'asdgsgfjfjkfdgasdhf', '2019-06-26', ''),
(53, 63, 'COMPUESTOS FURANOS', 'ABC', 2, 3, 'asdasdasdasdasd', '2019-06-14', 'cap210 (1).pdf'),
(54, 65, 'EXAFLORURO DE AZUFRE', 'ABC', 1, 2, 'Esto es Cuestionable', '2019-06-27', 'once_noticias_presupuesto-softech.pdf'),
(55, 61, 'SATURACION', 'ABC', 1, 2, 'ESTO ES CUESTIONABLE', '2019-06-01', 'once_noticias_presupuesto-softech.pdf'),
(56, 61, 'DENSIDAD RELATIVA', 'ABC', 2, 1, 'ESTO ES CRÍTICO', '2019-06-01', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf'),
(57, 61, 'RESISTENCIA DE AISLAMIENTO', 'ABC', 1, 1, 'Estamos en estado CRÍTICO', '2019-06-03', 'cap210 (1).pdf'),
(58, 65, 'RESISTENCIA OHMICA DE CONTACTOS', 'ABC', 1, 1, 'Estamos en un estado algo crítico. favor corroborar', '2019-06-08', '03. estimacion-presupuesto-rmedia-softech.pdf'),
(59, 68, 'RESISTENCIA DE AISLAMIENTO', 'ABC', 1, 3, 'No hay recomendaciones', '2019-06-29', 'cap210 (1).pdf'),
(60, 68, 'RESISTENCIA DE AISLAMIENTO', 'ABC', 1, 2, 'Los datos estan superiores a la media, favor hacer nueva revisión en 1 mes', '2019-06-29', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf'),
(61, 67, 'VISCOSIDAD', 'ABC', 2, 2, 'Presenta algunos trabones', '2019-06-27', ''),
(62, 69, 'RESISTENCIA DE AISLAMIENTO', 'ABC', 1, 3, 'En excelentes condiciones', '2019-06-24', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf'),
(63, 69, 'CONTENIDO AZUFRE CORROSIVO', 'ABC', 2, 3, 'En excelentes condiciones', '2019-06-25', 'once_noticias_presupuesto-softech.pdf'),
(64, 69, 'CONTENIDO PCBs', 'ABC', 2, 2, 'Lo cuestionable de esto fue', '2019-06-29', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol` varchar(20) NOT NULL,
  `fecha_reg` date NOT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `email`, `password`, `rol`, `fecha_reg`, `imagen`) VALUES
(1, 'Super', 'Administrador', 'admin@admin.com', '$2y$04$FxUXggZKD4wKysOMX1b8QunNYDkTR3iXgVCpucT32PKNx7Y.ggduC', 'admin', '2019-06-11', NULL),
(3, 'Marian', 'Maradiaga', 'oillab@energiapd.com', '$2y$04$rju2oyfb8kjwzzpqoaIcre0jZ8vKRcD9rsle.HeD7N/oRTCUNvi.O', 'admin', '2019-06-20', NULL),
(4, 'Daniel', 'Brizuela', 'dbrizuela@energiapd.com', '$2y$04$UxtIbR38qtB53BdX9XDD1eiFAG6fqFAuQvZ6mdncpvR/5mT61fiMm', 'admin', '2019-06-20', NULL),
(5, 'Claudia', 'Martinez', 'cmartinez@energiapd.com', '$2y$04$0oZEB97dh8jzWgh5PJf4UeKn/6m.rY9FJ1Dhb/C.mBOm7f051jTc2', 'admin', '2019-06-20', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_email` (`email`),
  ADD KEY `fk_cliente_admin` (`admin_id`);

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_equipo_proyecto` (`proyecto_id`);

--
-- Indices de la tabla `informes`
--
ALTER TABLE `informes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_informe_proyecto` (`proyect_id`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_proyecto_admin` (`admin_id`),
  ADD KEY `fk_proyecto_cliente` (`cliente_id`);

--
-- Indices de la tabla `pruebas`
--
ALTER TABLE `pruebas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_prueba_equipo` (`equipo_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT de la tabla `informes`
--
ALTER TABLE `informes`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `pruebas`
--
ALTER TABLE `pruebas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `fk_equipo_proyecto` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `informes`
--
ALTER TABLE `informes`
  ADD CONSTRAINT `fk_informe_proyecto` FOREIGN KEY (`proyect_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD CONSTRAINT `fk_proyecto_admin` FOREIGN KEY (`admin_id`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `pruebas`
--
ALTER TABLE `pruebas`
  ADD CONSTRAINT `fk_prueba_equipo` FOREIGN KEY (`equipo_id`) REFERENCES `equipos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
