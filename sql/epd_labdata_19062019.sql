-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 20-06-2019 a las 02:26:18
-- Versión del servidor: 5.7.17-log
-- Versión de PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `epd_labdata`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` int(10) NOT NULL,
  `admin_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol` varchar(100) NOT NULL,
  `empresa` varchar(100) DEFAULT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `vendedor` varchar(100) DEFAULT NULL,
  `fecha_reg` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `admin_id`, `nombre`, `apellidos`, `email`, `password`, `rol`, `empresa`, `ciudad`, `vendedor`, `fecha_reg`) VALUES
(1, 1, 'Allan', 'Samayoa', 'allan.samayoa@pixelmediahn.com', 'Shialebeouf30', '', 'Pixel Media', 'San Pedro Sula', 'Dayana Melendez', '2019-06-11'),
(2, 1, 'Josue', 'Fiallos', 'mercadeo@pixelmediahn.com', 'Shialebeouf30', '', 'Pixel Media', 'San Pedro Sula', 'Dayana Melendez', '2019-06-11'),
(3, 1, 'Leyla', 'Soto', 'leyla@leyla.com', 'leyla', '', 'LEYLAND', 'San Pedro Sula', 'Carmen Martinez', '2019-06-14'),
(4, 1, 'cliente', 'cliente', 'cliente@cliente.com', '$2y$04$Jb.ZI5qTTbkLiDkvvXYg/OrqNZRq8JLE8i7bPSJsAARphojHtpmMy', 'cliente', 'CLIENTEEMPRESA', 'SAN PEDRO SULA', 'Carmen Boquin', '2019-06-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id` int(255) NOT NULL,
  `proyecto_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `marca` varchar(100) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `serie` varchar(100) NOT NULL,
  `fabricante` varchar(100) NOT NULL,
  `descripcion` text,
  `fecha_crea` date NOT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `proyecto_id`, `nombre`, `marca`, `modelo`, `serie`, `fabricante`, `descripcion`, `fecha_crea`, `imagen`) VALUES
(17, 12, 'LOREM', 'IPSUN ', 'DOLOR ', 'SIT ', 'AMET', 'Lorem Ipsun Dolor sit amet', '2019-06-18', ''),
(18, 12, 'SELECCIONADORA', 'WIRPOOL', 'AS546D', '6546+a78s9d87a', 'asdasd', 'aSdasdASdasd', '2019-06-18', ''),
(19, 13, 'ASDASD', 'ASDASD', 'ASDASD', 'ASDASD', 'ASDASD', 'ASDASD', '2019-06-18', ''),
(20, 13, 'GKHJKG', 'HKGHJK', 'JHKLJKÑ', 'HDFGHGF', 'HDFGHDFG', 'DFGHDFGHDFGHDF', '2019-06-18', 'meme_martin.jpg'),
(21, 13, 'ASDASD', 'ASDASD', 'ASDASD', 'ASDASD', 'ASDASD', 'ASDASD', '2019-06-19', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informes`
--

CREATE TABLE `informes` (
  `id` int(255) NOT NULL,
  `proyect_id` int(255) NOT NULL,
  `equipo_id` int(255) NOT NULL,
  `serie` varchar(255) NOT NULL,
  `fecha_informe` date NOT NULL,
  `result_electricas` int(3) NOT NULL,
  `recom_electricas` varchar(255) DEFAULT NULL,
  `result_aceite` int(3) NOT NULL,
  `recom_aceite` varchar(255) DEFAULT NULL,
  `archivo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `informes`
--

INSERT INTO `informes` (`id`, `proyect_id`, `equipo_id`, `serie`, `fecha_informe`, `result_electricas`, `recom_electricas`, `result_aceite`, `recom_aceite`, `archivo`) VALUES
(7, 12, 18, '6546+a78s9d87a', '2019-06-18', 1, '                Ingrese sus recomendaciones\r\n                ', 2, '                Ingrese sus recomendaciones\r\n                ', ''),
(8, 13, 20, 'HDFGHGF', '2019-06-19', 2, '                Ingrese sus recomendacionesasdasdas\r\n                ', 3, '                Ingrese sus recomendaciones\r\n                asdasdasd', 'Instructivo.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `id` int(255) NOT NULL,
  `admin_id` int(255) NOT NULL,
  `cliente_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `fecha_crea` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`id`, `admin_id`, `cliente_id`, `nombre`, `ciudad`, `descripcion`, `fecha_crea`) VALUES
(12, 1, 4, 'Proyecto Nuevo', 'Choloma', 'Estos son los datos relevantes sobre el proyecto', '2019-06-18'),
(13, 1, 4, 'TRAFOS CHOLOMA ON', 'Choloma', 'Lorem Datos relevantes sobre el proyecto', '2019-06-18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pruebas`
--

CREATE TABLE `pruebas` (
  `id` int(255) NOT NULL,
  `equipo_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `abreviacion` varchar(100) NOT NULL,
  `tipo_prueba` int(2) NOT NULL,
  `resultado` int(3) NOT NULL,
  `recomend` varchar(255) NOT NULL,
  `fecha_prueba` date NOT NULL,
  `archivo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pruebas`
--

INSERT INTO `pruebas` (`id`, `equipo_id`, `nombre`, `abreviacion`, `tipo_prueba`, `resultado`, `recomend`, `fecha_prueba`, `archivo`) VALUES
(32, 21, 'RESPUESTA DIELECTRICA EN EL DOMINIO DE LA FRECUENCIA', 'ABC', 1, 1, '', '2019-06-19', 'Instructivo.pdf'),
(33, 21, 'INDICE DE ACIDEZ', 'ABC', 2, 2, '            De acuerdo a los resultados se recomienda: \r\n            Deberian mandar el equipo a revision de nuevo', '2019-06-19', ''),
(34, 21, 'TIEMPO DE PERTURA Y CIERRE', 'ABC', 1, 2, '            De acuerdo a los resultados se recomienda: \r\n            Lorempisun', '2019-06-12', 'Instructivo.pdf'),
(35, 20, 'CONTENIDO PCBs', 'ABC', 2, 1, '            De acuerdo a los resultados se recomienda: \r\n            asdasdasd', '2019-06-19', 'Instructivo.pdf'),
(36, 18, 'SATURACION', 'ABC', 1, 3, '            De acuerdo a los resultados se recomienda: \r\n            asdasd', '2019-06-19', 'Instructivo.pdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol` varchar(20) NOT NULL,
  `fecha_reg` date NOT NULL,
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `email`, `password`, `rol`, `fecha_reg`, `imagen`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', '$2y$04$FxUXggZKD4wKysOMX1b8QunNYDkTR3iXgVCpucT32PKNx7Y.ggduC', 'admin', '2019-06-11', NULL),
(2, 'cliente', 'cliente', 'cliente@cliente', '$2y$04$tDCFChHp2GtRur.5loVMBuHDbEsZ2k0wvR9I5iDVj4fyM6MuRqfDC', 'basic', '2019-06-11', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_email` (`email`),
  ADD KEY `fk_cliente_admin` (`admin_id`);

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_equipo_proyecto` (`proyecto_id`);

--
-- Indices de la tabla `informes`
--
ALTER TABLE `informes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_informe_proyecto` (`proyect_id`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_proyecto_admin` (`admin_id`),
  ADD KEY `fk_proyecto_cliente` (`cliente_id`);

--
-- Indices de la tabla `pruebas`
--
ALTER TABLE `pruebas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_prueba_equipo` (`equipo_id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uq_email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `informes`
--
ALTER TABLE `informes`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `pruebas`
--
ALTER TABLE `pruebas`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `fk_cliente_admin` FOREIGN KEY (`admin_id`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `fk_equipo_proyecto` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `informes`
--
ALTER TABLE `informes`
  ADD CONSTRAINT `fk_informe_proyecto` FOREIGN KEY (`proyect_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD CONSTRAINT `fk_proyecto_admin` FOREIGN KEY (`admin_id`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `fk_proyecto_cliente` FOREIGN KEY (`cliente_id`) REFERENCES `clientes` (`id`);

--
-- Filtros para la tabla `pruebas`
--
ALTER TABLE `pruebas`
  ADD CONSTRAINT `fk_prueba_equipo` FOREIGN KEY (`equipo_id`) REFERENCES `equipos` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
