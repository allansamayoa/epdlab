/*
Navicat MySQL Data Transfer

Source Server         : epdLab
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : epd_labdata

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-09-29 22:00:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for catalog_pruebas
-- ----------------------------
DROP TABLE IF EXISTS `catalog_pruebas`;
CREATE TABLE `catalog_pruebas` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `abreviacion` varchar(100) NOT NULL,
  `tipo_prueba` int(2) NOT NULL,
  `fecha_crea` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of catalog_pruebas
-- ----------------------------
INSERT INTO `catalog_pruebas` VALUES ('1', 'RESISTENCIA DE AISLAMIENTO', 'RA', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('2', 'FACTOR DE POTENCIA', 'FP', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('3', 'CORRIENTE EXCITACIÓN', 'I EXC', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('4', 'RESISTENCIA DE DEVANADO', 'RD', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('5', 'RELACION DE TRANSFORMACION', 'TTR', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('6', 'IMPEDANCIA DE CORTO CIRCUITO', 'ZCC', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('7', 'RESPUESTA AL BARRIDO DE LA FRECUENCIA', 'SFRA', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('8', 'RIGIDEZ DIELECTRICA', 'RIGIDEZ', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('9', 'PUNTO DE ROCIO', 'PR', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('10', 'TIP UP', 'TU', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('11', 'SATURACION', 'SAT', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('12', 'RESISTENCIA OHMICA DE CONTACTOS', 'RC', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('13', 'TIEMPO DE PERTURA Y CIERRE', 'OC', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('14', 'EXAFLORURO DE AZUFRE', 'SF6', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('15', 'TERMOGRAFIA', 'TER', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('16', 'MUY BAJA FRECUENCIA', 'VLF', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('17', 'RESPUESTA DIELECTRICA EN EL DOMINIO DE LA FRECUENCIA', 'DFR', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('18', 'POLARIDAD', 'POL', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('19', 'BURDEN', 'BUR', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('20', 'INYECCION SECUNDARIA', 'IN SEC', '1', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('21', 'DGA-CAMPO', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('22', 'RIGIDEZ DIELECTRICA', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('23', 'KARL FISHER', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('24', 'FP LIQUIDO', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('25', 'INDICE DE ACIDEZ', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('26', 'COLOR E INSPECCION VISUAL', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('27', 'COMPUESTOS FURANOS', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('28', 'TENSION INTERFACIAL', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('29', 'DENSIDAD RELATIVA', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('30', 'CONTENIDO INHIBIDO OXIDACION', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('31', 'METALES DISUELTOS', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('32', 'CONTENIDO PCBs', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('33', 'CONTENIDO AZUFRE CORROSIVO', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('34', 'VISCOSIDAD', '???', '2', '2019-08-29');
INSERT INTO `catalog_pruebas` VALUES ('35', 'PUNTO DE ANILINA', '???', '2', '2019-08-29');

-- ----------------------------
-- Table structure for equipos
-- ----------------------------
DROP TABLE IF EXISTS `equipos`;
CREATE TABLE `equipos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `proyecto_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `marca` varchar(100) NOT NULL,
  `modelo` varchar(100) NOT NULL,
  `serie` varchar(100) NOT NULL,
  `fabricante` varchar(100) NOT NULL,
  `descripcion` text DEFAULT NULL,
  `fecha_crea` date NOT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_equipo_proyecto` (`proyecto_id`),
  CONSTRAINT `fk_equipo_proyecto` FOREIGN KEY (`proyecto_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of equipos
-- ----------------------------
INSERT INTO `equipos` VALUES ('56', '14', 'EQUIPO MARIANT', 'SHELL', 'EQUIMA', '85874EQ', 'SHELL', 'Lorem ipsun dolor sit amet donec', '2019-06-27', 'BAGU4157 (1).JPG');
INSERT INTO `equipos` VALUES ('57', '14', 'EQUIPO TESTO', 'ROLAND', 'TERT', 'RTYY8822', 'ROLAND', 'More than we can imagine', '2019-06-27', 'FBRZ2073 (1).JPG');
INSERT INTO `equipos` VALUES ('58', '27', 'EQUIPO DE TESTEO', 'TESTA MOTOR CO.', 'TE99', 'TEQ-254781', 'TESTA MOTOR CO.', 'Equipo de prueba para ensayos de la herramienta.', '2019-06-27', '63000-3ranges-1.jpg');
INSERT INTO `equipos` VALUES ('59', '27', 'PLANTA ELECTRICA', 'SHERWING WILLIAMS', 'TP8000', 'TPL-857412', 'SHERWING WILLIAMS', 'Una planta interesante para la aplicacion de novedades', '2019-06-27', 'planta-energia-rdrental-02.jpg');
INSERT INTO `equipos` VALUES ('60', '28', 'DRON MAVERIC', 'TESTLA', 'MD-8765', '542100', 'TESTLA', 'Lorem Ipsun dolro sit amet', '2019-06-27', 'electroimanes-accionamiento.jpg');
INSERT INTO `equipos` VALUES ('61', '28', 'TELEFONO PLANTADOS', 'BELL COMPUTERS', 'BELL', 'BELL-857412', 'BELL COMPUTERS', 'El telefono especial para todo tipo de llamadsa', '2019-06-27', 'telefono-antiguo.jpg');
INSERT INTO `equipos` VALUES ('62', '28', 'BOMBA AGUA AZUL', 'BOMOSA S:A:', 'BOMOBLUE', 'BM857412', 'BOMOSA S:A:', 'La bomba de agua más potente de la región', '2019-06-27', '380.jpg');
INSERT INTO `equipos` VALUES ('63', '29', 'TRANSFORMADOR', 'EEH COLOMBIA', 'TR2000', 'TRAFO-857412', 'EEH COLOMBIA', 'Un transformador espectacular con la capacidad de 25kw por hora de procesamiento.', '2019-06-27', '20368787-transformador-viejo-en-poste-eléctrico.jpg');
INSERT INTO `equipos` VALUES ('64', '29', 'SELECCIONADORA', 'AMBLIM ENT.', 'METALGEAR', 'MG-857412', 'AMBLIM ENT.', 'Esta seleccionadora sirve para seleccionar todo lo seleccionable en una seleccion natural.', '2019-06-27', 'Selezionatrice-per-uova-S91.jpg');
INSERT INTO `equipos` VALUES ('65', '28', 'COSA', 'jjashd', 'CASDA', 'asds', 'jjashd', 'jsdghaoslhdphqdf', '2019-06-28', 'Selezionatrice-per-uova-S91.jpg');
INSERT INTO `equipos` VALUES ('66', '30', 'TRANSFORMADOR', 'WESTINGHOUSE', 'T-857', 'TRF-547893', 'WESTINGHOUSE', 'Este transformador tiene la capacidad de procesar 25kw al día', '2019-06-29', '20368787-transformador-viejo-en-poste-eléctrico.jpg');
INSERT INTO `equipos` VALUES ('67', '30', 'SELECCIONADORA', 'TORNOS MANUEL', 'SC7874', '874568578898', 'TORNOS MANUEL', 'Puede procesar 50 kms de material por hora.', '2019-06-29', 'Selezionatrice-per-uova-S91.jpg');
INSERT INTO `equipos` VALUES ('68', '30', 'SINCRONIZADOR', 'SONY CORP', 'ST987', '665464656354HDFG74654', 'SONY INC.', 'Capaz de tomar frecuencias de todas las estaciones radiales del mundo', '2019-06-29', 'electroimanes-accionamiento.jpg');
INSERT INTO `equipos` VALUES ('71', '32', 'SWITCHER', 'SONY CORP', 'M6000', 'BM857412', 'SONY CORP', 'Un switcher de producción audiovisual para cámaras HD', '2019-08-29', 'dffe56b04ea5cb14e60274888924a6d1.jpg');
INSERT INTO `equipos` VALUES ('72', '32', 'TASTKAM', 'Taskam INC.', 'T3333', 'TSK500', 'Taskam INC.', 'Grabadora de audio', '2019-08-31', '');

-- ----------------------------
-- Table structure for informes
-- ----------------------------
DROP TABLE IF EXISTS `informes`;
CREATE TABLE `informes` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `proyect_id` int(255) NOT NULL,
  `equipo_id` int(255) NOT NULL,
  `serie` varchar(255) NOT NULL,
  `fecha_informe` date NOT NULL,
  `result_electricas` int(3) NOT NULL,
  `recom_electricas` varchar(255) DEFAULT NULL,
  `result_aceite` int(3) NOT NULL,
  `recom_aceite` varchar(255) DEFAULT NULL,
  `archivo` text DEFAULT NULL,
  `archivo_pa` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_informe_proyecto` (`proyect_id`),
  CONSTRAINT `fk_informe_proyecto` FOREIGN KEY (`proyect_id`) REFERENCES `proyectos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of informes
-- ----------------------------
INSERT INTO `informes` VALUES ('21', '14', '57', 'RTYY8822', '2019-06-26', '3', '                Ingrese sus recomendaciones\r\n                ', '1', '                Ingrese sus recomendaciones\r\n                ', '03. estimacion-presupuesto-rmedia-softech.pdf', null);
INSERT INTO `informes` VALUES ('22', '14', '56', '85874EQ', '2019-06-26', '1', '                Ingrese sus recomendaciones\r\n                ', '2', '                Ingrese sus recomendaciones\r\n                ', 'cap210 (1).pdf', null);
INSERT INTO `informes` VALUES ('23', '14', '57', 'RTYY8822', '2019-06-27', '1', '                Ingrese sus recomendaciones\r\n                ', '1', '                Ingrese sus recomendaciones\r\n                ', 'once_noticias_presupuesto-softech.pdf', null);
INSERT INTO `informes` VALUES ('24', '27', '59', 'TPL-857412', '2019-06-26', '2', '                Ingrese sus recomendaciones\r\n                ', '1', '                Ingrese sus recomendaciones\r\n                ', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf', null);
INSERT INTO `informes` VALUES ('36', '29', '64', 'MG-857412', '2019-06-28', '3', 'asdfasdfasfa', '1', 'asdfasdfasdf', 'cap210 (1).pdf', null);
INSERT INTO `informes` VALUES ('37', '29', '63', 'TRAFO-857412', '2019-06-23', '2', 'fasdggdhfg', '3', 'safdhgkgasd', 'once_noticias_presupuesto-softech.pdf', null);
INSERT INTO `informes` VALUES ('49', '28', '61', 'BELL-857412', '2019-06-27', '3', 'Estamos en un estado ACEPTABLE', '2', 'Estamos en un estsado CUESTIONABLE', 'cap210 (1).pdf', null);
INSERT INTO `informes` VALUES ('50', '30', '68', '665464656354HDFG74654', '2019-06-29', '1', 'Revisar una prueba crítica', '3', 'N/A', '03. estimacion-presupuesto-rmedia-softech.pdf', null);
INSERT INTO `informes` VALUES ('53', '32', '71', 'BM857412', '2019-08-30', '3', 'asdasdasd', '2', 'asdasdasd', 'CONSTANCIA DE TRABAJO.pdf', null);

-- ----------------------------
-- Table structure for proyectos
-- ----------------------------
DROP TABLE IF EXISTS `proyectos`;
CREATE TABLE `proyectos` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `admin_id` int(255) NOT NULL,
  `cliente_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `descripcion` text DEFAULT NULL,
  `fecha_crea` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proyecto_admin` (`admin_id`),
  KEY `fk_proyecto_cliente` (`cliente_id`),
  CONSTRAINT `fk_proyecto_admin` FOREIGN KEY (`admin_id`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of proyectos
-- ----------------------------
INSERT INTO `proyectos` VALUES ('14', '3', '4', 'PRUEBA MARIAN', 'SAP', 'Estos son los datos relevantes sobre el proyecto', '2019-06-20');
INSERT INTO `proyectos` VALUES ('27', '1', '9', 'TEST PRO', 'Cuyamel', 'Lorem ipsun dolor sit amet.', '2019-06-27');
INSERT INTO `proyectos` VALUES ('28', '1', '9', 'OPC  PUERTO', 'Puerto Cortés, Cortés', 'La instalacion de una aduana general', '2019-06-27');
INSERT INTO `proyectos` VALUES ('29', '3', '4', 'ELCATEX', 'Cholom', 'Proceso de grabacion de video', '2019-06-27');
INSERT INTO `proyectos` VALUES ('30', '4', '7', 'PLANTA DE TRATAMIENTO', 'Choloma, Cortés', 'Esta planta pretende hacer el tratamiento mensual de 27 toneladas de agua', '2019-06-29');
INSERT INTO `proyectos` VALUES ('32', '1', '7', 'EDIFICIO PIXEL MEDIA', 'SAN PEDRO SULA', 'Los estudios de televisión más grandes de la región', '2019-08-29');
INSERT INTO `proyectos` VALUES ('33', '1', '8', 'Pruebas Desarrollo', 'SPS', 'Pruebas desarrollo', '2019-09-27');

-- ----------------------------
-- Table structure for pruebas
-- ----------------------------
DROP TABLE IF EXISTS `pruebas`;
CREATE TABLE `pruebas` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `equipo_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `abreviacion` varchar(100) NOT NULL,
  `tipo_prueba` int(2) NOT NULL,
  `resultado` int(3) NOT NULL,
  `recomend` varchar(255) NOT NULL,
  `fecha_prueba` date NOT NULL,
  `archivo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_prueba_equipo` (`equipo_id`),
  CONSTRAINT `fk_prueba_equipo` FOREIGN KEY (`equipo_id`) REFERENCES `equipos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pruebas
-- ----------------------------
INSERT INTO `pruebas` VALUES ('43', '59', 'MUY BAJA FRECUENCIA', 'ABC', '1', '2', '            De acuerdo a los resultados se recomienda: \r\n            ', '2019-06-27', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf');
INSERT INTO `pruebas` VALUES ('44', '59', 'COMPUESTOS FURANOS', 'ABC', '2', '3', '            De acuerdo a los resultados se recomienda: \r\n            ', '2019-06-27', 'once_noticias_presupuesto-softech.pdf');
INSERT INTO `pruebas` VALUES ('45', '58', 'TIP UP', 'ABC', '1', '2', '            De acuerdo a los resultados se recomienda: \r\n            ', '2019-06-27', 'cap210 (1).pdf');
INSERT INTO `pruebas` VALUES ('47', '62', 'COLO E INSPECCION VISUAL', 'ABC', '2', '2', 'Esta todo tranquilo pero cuestionable', '2019-06-26', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf');
INSERT INTO `pruebas` VALUES ('48', '62', 'EXAFLORURO DE AZUFRE', 'ABC', '1', '3', 'Muy bien todo por este departamento', '2019-06-26', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf');
INSERT INTO `pruebas` VALUES ('49', '57', 'POLRIDAD', 'ABC', '1', '1', 'La polaridad de estas 2 fuerzas es diametralmente opuesta. Mantenganse alejados.', '2019-06-21', 'cap210 (1).pdf');
INSERT INTO `pruebas` VALUES ('50', '56', 'INDICE DE ACIDEZ', 'ABC', '2', '3', 'Todos son unos ÁCIDOS por lo que andan super bien', '2019-06-26', 'cap210 (1).pdf');
INSERT INTO `pruebas` VALUES ('51', '64', 'MUY BAJA FRECUENCIA', 'ABC', '1', '3', 'asdasdasd', '2019-06-26', '');
INSERT INTO `pruebas` VALUES ('52', '64', 'TERMOGRAFIA', 'ABC', '1', '3', 'asdgsgfjfjkfdgasdhf', '2019-06-26', '');
INSERT INTO `pruebas` VALUES ('53', '63', 'COMPUESTOS FURANOS', 'ABC', '2', '3', 'asdasdasdasdasd', '2019-06-14', 'cap210 (1).pdf');
INSERT INTO `pruebas` VALUES ('54', '65', 'EXAFLORURO DE AZUFRE', 'ABC', '1', '2', 'Esto es Cuestionable', '2019-06-27', 'once_noticias_presupuesto-softech.pdf');
INSERT INTO `pruebas` VALUES ('55', '61', 'SATURACION', 'ABC', '1', '2', 'ESTO ES CUESTIONABLE', '2019-06-01', 'once_noticias_presupuesto-softech.pdf');
INSERT INTO `pruebas` VALUES ('56', '61', 'DENSIDAD RELATIVA', 'ABC', '2', '1', 'ESTO ES CRÍTICO', '2019-06-01', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf');
INSERT INTO `pruebas` VALUES ('57', '61', 'RESISTENCIA DE AISLAMIENTO', 'ABC', '1', '1', 'Estamos en estado CRÍTICO', '2019-06-03', 'cap210 (1).pdf');
INSERT INTO `pruebas` VALUES ('58', '65', 'RESISTENCIA OHMICA DE CONTACTOS', 'ABC', '1', '1', 'Estamos en un estado algo crítico. favor corroborar', '2019-06-08', '03. estimacion-presupuesto-rmedia-softech.pdf');
INSERT INTO `pruebas` VALUES ('59', '68', 'RESISTENCIA DE AISLAMIENTO', 'ABC', '1', '3', 'No hay recomendaciones', '2019-06-29', 'cap210 (1).pdf');
INSERT INTO `pruebas` VALUES ('60', '68', 'RESISTENCIA DE AISLAMIENTO', 'ABC', '1', '2', 'Los datos estan superiores a la media, favor hacer nueva revisión en 1 mes', '2019-06-29', 'COTIZACION SESION FOTOGRAFICA IKAL COFFE.pdf');
INSERT INTO `pruebas` VALUES ('61', '67', 'VISCOSIDAD', 'ABC', '2', '2', 'Presenta algunos trabones', '2019-06-27', '');
INSERT INTO `pruebas` VALUES ('68', '71', 'PUNTO DE ROCIO', 'PR', '1', '2', 'CUestion', '2019-08-24', 'CONSTANCIA DE TRABAJO_VR02.pdf');
INSERT INTO `pruebas` VALUES ('69', '71', 'DGA-CAMPO', '???', '2', '3', 'Ya mejoro todo', '2019-08-23', 'Propuesta Producción Calle 7 Outsourcing VR 03.pdf');
INSERT INTO `pruebas` VALUES ('70', '72', 'CONTENIDO INHIBIDO OXIDACION', '???', '2', '2', 'CUESTIONABLE', '2019-08-30', 'CONSTANCIA DE TRABAJO_CORRECTED.pdf');
INSERT INTO `pruebas` VALUES ('71', '72', 'IMPEDANCIA DE CORTO CIRCUITO', 'ZCC', '1', '3', 'Aceptable', '2019-08-25', 'CONSTANCIA DE TRABAJO.pdf');
INSERT INTO `pruebas` VALUES ('72', '72', 'METALES DISUELTOS', '???', '2', '3', 'ac', '2019-08-24', 'CONSTANCIA DE TRABAJO.pdf');
INSERT INTO `pruebas` VALUES ('73', '72', 'MUY BAJA FRECUENCIA', 'VLF', '1', '3', 'asdasd', '2019-08-30', 'CONSTANCIA DE TRABAJO.pdf');
INSERT INTO `pruebas` VALUES ('74', '72', 'IMPEDANCIA DE CORTO CIRCUITO', 'ZCC', '1', '3', 'asdasd', '2019-08-31', 'CONSTANCIA DE TRABAJO.pdf');
INSERT INTO `pruebas` VALUES ('75', '72', 'VISCOSIDAD', '???', '2', '3', 'asdasd', '2019-08-21', 'CONSTANCIA DE TRABAJO.pdf');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `admin_id` int(255) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol` varchar(100) NOT NULL,
  `empresa` varchar(100) DEFAULT NULL,
  `ciudad` varchar(100) DEFAULT NULL,
  `vendedor` varchar(100) DEFAULT NULL,
  `fecha_reg` date NOT NULL,
  `imagen` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_email` (`email`),
  KEY `fk_cliente_admin` (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', '0', 'Super', 'Administrador', 'admin@admin.com', '$2y$04$FxUXggZKD4wKysOMX1b8QunNYDkTR3iXgVCpucT32PKNx7Y.ggduC', 'admin', null, null, null, '2019-06-11', null);
INSERT INTO `usuarios` VALUES ('3', '0', 'Marian', 'Maradiaga', 'oillab@energiapd.com', '$2y$04$lT1WUzriFu0209uHh.nF1.0Gnvzjgb6KzhjIrqDhAMCt.5gSwTkuK', 'basic', null, null, null, '2019-06-20', null);
INSERT INTO `usuarios` VALUES ('4', '0', 'Daniel', 'Brizuela', 'dbrizuela@energiapd.com', '$2y$04$UxtIbR38qtB53BdX9XDD1eiFAG6fqFAuQvZ6mdncpvR/5mT61fiMm', 'admin', null, null, null, '2019-06-20', null);
INSERT INTO `usuarios` VALUES ('5', '0', 'Claudia', 'Martinez', 'cmartinez@energiapd.com', '$2y$04$0oZEB97dh8jzWgh5PJf4UeKn/6m.rY9FJ1Dhb/C.mBOm7f051jTc2', 'admin', null, null, null, '2019-06-20', null);
INSERT INTO `usuarios` VALUES ('7', '4', 'Pedro', 'Abarca', 'pedro@pedro.com', '$2y$04$5VKPxAyRiZII68dIWuHEueZyY14IsO9tUL9Icw3O2uzbs/wEDH6lS', 'cliente', 'PETER CORP', 'San Pedro Sula', 'Claudia Martinez', '2019-06-29', null);
INSERT INTO `usuarios` VALUES ('8', '1', 'Cliente', 'Ejemplo', 'cliente@cliente.com', '$2y$04$5Gveqg5LaWLm2y4/3yzlfuRnhUL1.mYRSge.0JY2sl6q6B4NYdjTG', 'cliente', 'EMPRESA MODELO', 'SAN PEDRO SULA', 'Carmen Boquin', '2019-06-14', null);
INSERT INTO `usuarios` VALUES ('9', '1', 'Allan ', 'Samayoa', 'asamayoa@pixelmediahn.com', '$2y$04$wuMHpeOJ3DW1MKmhRH2FeOe0h/t7tqdstA.Ew72rb.MQbNyzW4B5e', 'cliente', 'PIXEL MEDIA', 'SAN PEDRO SULA', 'Claudia Martinez', '2019-06-29', null);
