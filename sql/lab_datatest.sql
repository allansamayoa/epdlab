/*
 * Archivo de creacion de base de datos desde CERO
 * Con algunos ingresos de datos de muestra.
 */

CREATE DATABASE epd_labdata;
USE epd_labdata;

CREATE TABLE usuarios(
    id          INT(10)         auto_increment  not null,
    nombre      varchar(100) not null, 
    apellidos   varchar(100),
    email       varchar(255) not null, 
    password    varchar(255) not null,
    rol         varchar(20) not null,
    fecha_reg   date not null,
    imagen      varchar(255),
    CONSTRAINT pk_usuarios PRIMARY KEY(id),
    CONSTRAINT uq_email UNIQUE(email)
)ENGINE=InnoDB;

INSERT INTO usuarios VALUES(NULL, 'Admin', 'Admin', 'webservices@pixelmediahn.com', 'PixelWebMaster2k18$', 'admin', CURDATE(), NULL);

CREATE TABLE clientes(
    id          INT(10)         auto_increment not null,
    admin_id    INT(255)        not null,
    nombre      varchar(100)    not null, 
    apellidos   varchar(100), 
    email       varchar(255)    not null, 
    password    varchar(255)    not null, 
    empresa     varchar(100),
    ciudad      varchar(100),
    vendedor    varchar(100),
    fecha_reg   date not null, 
    CONSTRAINT pk_clientes PRIMARY KEY(id),
    CONSTRAINT fk_cliente_admin FOREIGN KEY(admin_id) REFERENCES usuarios(id),
    CONSTRAINT uq_email UNIQUE(email)
)ENGINE=InnoDB;

INSERT INTO clientes VALUES(NULL, '1', 'Allan', 'Samayoa', 'allan.samayoa@gmail.com', 'Shialebeouf30', 'Familiar', 'San Pedro Sula', 'Dayana Melendez', CURDATE());
INSERT INTO clientes VALUES(NULL, '1', 'Josue', 'Fiallos', 'mercadeo@pixelmediahn.com', 'LoremIpsunDolor2k19$', 'PixelMedia', 'San Pedro Sula', 'Dayana Melendez', CURDATE());

CREATE TABLE proyectos(
    id          INT(255)    auto_increment not null,
    admin_id    INT(255)     not null,
    cliente_id  INT(255)     not null,
    nombre      varchar(100) not null,
    ciudad      varchar(100),
    descripcion text,
    fecha_crea  date           not null, 
    CONSTRAINT  pk_proyectos PRIMARY KEY(id),
    CONSTRAINT  fk_proyecto_admin FOREIGN KEY(admin_id) REFERENCES usuarios(id),
    CONSTRAINT  fk_proyecto_cliente FOREIGN KEY(cliente_id) REFERENCES clientes(id)
)ENGINE=InnoDb;

INSERT INTO proyectos VALUES(NULL, 5, 1, 'Subestacion Bermejo', 'San Pedro Sula', 'Una subestacion con la capacidad de 50KW por hora', CURDATE());
INSERT INTO proyectos VALUES(NULL, 5, 1, 'Termica Occidente', 'Santa Rosa de Copan', 'La termica que brinda energia a todo Copán', CURDATE());
INSERT INTO proyectos VALUES(NULL, 5, 1, 'Represa el Coyolito', 'Yoro', 'Represa capaz de llevar 25kw hora', CURDATE());
INSERT INTO proyectos VALUES(NULL, 5, 1, 'Estación Solar Nacaome', 'Nacaome, Valle', 'Granja Solar de 90 Kms2 produciendo 10Kw por Km2', CURDATE());


CREATE TABLE equipos(
    id          INT(255)    auto_increment not null,
    proyecto_id INT(255)     not null,
    nombre      varchar(100) not null,
    marca       varchar(100) not null,
    modelo      varchar(100) not null,
    serie       varchar(100) not null,
    fabricante  varchar(100) not null,
    descripcion text,
    fecha_crea  date not null, 
    imagen      varchar(255),
    CONSTRAINT  pk_equipos PRIMARY KEY(id),
    CONSTRAINT  fk_equipo_proyecto FOREIGN KEY(proyecto_id) REFERENCES proyectos(id) ON DELETE CASCADE
)ENGINE=InnoDb;



INSERT INTO equipos VALUES(NULL, 1, 'TRANSFORMADOR', 'GENERAL ELECTRIC', 'PEGASO', 'KGB-254012', 'GE-JAPON', 'El transformador de la nueva generación', CURDATE(), null);
INSERT INTO equipos VALUES(NULL, 1, 'SELECCIONADORA', 'WESTINGHOUSE', 'ROCKETEER', 'CIA-841229', 'WH-USA', 'La seleccionadora más eficiente del mercado', CURDATE(), null);

CREATE TABLE informes(
    id                  INT(255)        auto_increment not null,
    proyect_id          INT(255)        not null,
    equipo_id           INT(255)        not null,
    serie               VARCHAR (255)   not null,
    fecha_informe       date            not null, 
    result_electricas   INT(3)          not null,
    recom_electricas    VARCHAR (255),
    result_aceite       INT(3)          not null,
    recom_aceite        VARCHAR (255),
    archivo             VARCHAR (255),
    CONSTRAINT  pk_informes PRIMARY KEY(id),
    CONSTRAINT  fk_informe_proyecto FOREIGN KEY(proyect_id) REFERENCES proyectos(id) ON DELETE CASCADE
)ENGINE=InnoDb;

INSERT INTO informes VALUES(NULL, 1, 10, 'FAM-741234', CURDATE(), 1, 'Lorem ipsun dolor sit amet', 2, 'Lorem ipsun dolor sit amet', "muestra.pdf");
INSERT INTO informes VALUES(NULL, 1, 9, 'PZT-577741', CURDATE(), 1, 'Lorem ipsun dolor sit amet', 2, 'Lorem ipsun dolor sit amet', "muestra.pdf");
INSERT INTO informes VALUES(NULL, 1, 2, 'CIA-841229', CURDATE(), 1, 'Lorem ipsun dolor sit amet', 2, 'Lorem ipsun dolor sit amet', "muestra.pdf");

CREATE TABLE pruebas(
    id                  INT(255)        auto_increment not null,
    equipo_id           INT(255)        not null,
    nombre              varchar(100)    not null,
    abreviacion         varchar(100)    not null,
    tipo_prueba         INT(2)          not null,
    resultado           INT(3)          not null,
    fecha_prueba        date            not null,
    archivo             VARCHAR (255)   not null,
    CONSTRAINT  pk_pruebas PRIMARY KEY(id),
    CONSTRAINT  fk_prueba_equipo FOREIGN KEY(equipo_id) REFERENCES equipos(id) ON DELETE CASCADE
)ENGINE=InnoDb;

INSERT INTO pruebas VALUES(NULL, 2, 'RESISTENCIA DE AISLAMIENTO', 'RA', 1, 1, CURDATE(), "muestra.pdf");
INSERT INTO pruebas VALUES(NULL, 2, 'FACTOR DE POTENCIA', 'FP', 1, 2, CURDATE(), "muestra.pdf");
INSERT INTO pruebas VALUES(NULL, 6, 'CORRIENTE EXCITACIÓN', 'I EXC', 2, 1, CURDATE(), "muestra.pdf");
INSERT INTO pruebas VALUES(NULL, 6, 'RESISTENCIA DE DEVANADO', 'RD', 2, 3, CURDATE(), "muestra.pdf");


CREATE TABLE catalog_pruebas(
    id                  INT(255)        auto_increment not null,
    nombre              varchar(100)    not null,
    abreviacion         varchar(100)    not null,
    tipo_prueba         INT(2)          not null,
    fecha_crea          date            not null,
    CONSTRAINT  pk_pruebas PRIMARY KEY(id)
)ENGINE=InnoDb;


INSERT INTO catalog_pruebas VALUES(NULL, 'RESISTENCIA DE AISLAMIENTO', 'RA', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'FACTOR DE POTENCIA', 'FP', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'CORRIENTE EXCITACIÓN', 'I EXC', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'RESISTENCIA DE DEVANADO', 'RD', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'RELACION DE TRANSFORMACION', 'TTR', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'IMPEDANCIA DE CORTO CIRCUITO', 'ZCC', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'RESPUESTA AL BARRIDO DE LA FRECUENCIA', 'SFRA', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'RIGIDEZ DIELECTRICA', 'RIGIDEZ', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'PUNTO DE ROCIO', 'PR', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'TIP UP', 'TU', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'SATURACION', 'SAT', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'RESISTENCIA OHMICA DE CONTACTOS', 'RC', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'TIEMPO DE PERTURA Y CIERRE', 'OC', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'EXAFLORURO DE AZUFRE', 'SF6', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'TERMOGRAFIA', 'TER', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'MUY BAJA FRECUENCIA', 'VLF', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'RESPUESTA DIELECTRICA EN EL DOMINIO DE LA FRECUENCIA', 'DFR', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'POLARIDAD', 'POL', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'BURDEN', 'BUR', 1, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'INYECCION SECUNDARIA', 'IN SEC', 1, CURDATE());

INSERT INTO catalog_pruebas VALUES(NULL, 'DGA-CAMPO', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'RIGIDEZ DIELECTRICA', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'KARL FISHER', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'FP LIQUIDO', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'INDICE DE ACIDEZ', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'COLOR E INSPECCION VISUAL', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'COMPUESTOS FURANOS', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'TENSION INTERFACIAL', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'DENSIDAD RELATIVA', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'CONTENIDO INHIBIDO OXIDACION', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'METALES DISUELTOS', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'CONTENIDO PCBs', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'CONTENIDO AZUFRE CORROSIVO', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'VISCOSIDAD', '???', 2, CURDATE());
INSERT INTO catalog_pruebas VALUES(NULL, 'PUNTO DE ANILINA', '???', 2, CURDATE());




