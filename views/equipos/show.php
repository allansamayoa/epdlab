<!-- **********************  SECCION PARA ADMINS  ****************************** -->

<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "admin") : ?>
        <?php   
            if($_GET['proid']){
                $proid = $_GET['proid'];
            }
        ?>

            <div id="portada">
                <h2>Datos del equipo:</h2>
                    <?php if(isset($equi)): ?>
                        <?php if($equi->imagen !=null): ?>
                            <img src="<?=base_url?>uploads/images/<?=$equi->imagen?>" class="thumb"/>
                        <?php else: ?>
                        <img src="<?=base_url?>uploads/images/equipo_generico.jpg" />  
                    <?php endif; ?>
            </div>
            <div id="infohead">
                <h1><?=$equi->nombre." - ".$equi->serie?></h1> 
                <h3>Marca: <?=$equi->marca." - Modelo: ".$equi->modelo?></h3>
                <p><?=$equi->descripcion?></p>
                    <?php $proyecto = Utilities::showCurrentProid(); ?>
                        <h3>Proyecto al que pertenece: 
                            <a href="<?=base_url?>proyecto/show&id=<?=$proyecto->id?>" class="boton boton-blue">
                                <---Volver a: <?=$proyecto->nombre?>
                            </a>
                        </h3>   
                        <a href="<?=base_url?>equipo/editar&proid=<?=$proid?>&id=<?=$equi->id?>" class="boton boton-orange">
                                Editar equipo 
                            </a>                 
            </div>
                <?php else: ?>
                    <h1>El Equipo no existe</h1>
                <?php endif; ?>

        <?php
            if($_GET['id']){
                $id = $_GET['id'];
            }
        ?>
        <div class="content-hijo">
            <h1>Gestion de Pruebas Eléctricas y al Aceite</h1>
            <a href="<?=base_url?>prueba/registrarByEqui&proid=<?=$proid?>&equid=<?=$id?>" class="boton boton-green">Agregar Prueba</a>
            <div class="data-table">
                <table border="1">
                    <tr>
                        <th>FECHA</th>
                        <th>NOMBRE</th>
                        <th>ABREV</th>
                        <th>TIPO</th>
                        <th>RESULTADO</th>
                        <th>RECOMENDACION</th>
                        <th>ARCHIVO</th>
                        <th>ACCION</th>
                    </tr>
                    <?php $pruebas = Utilities::getPruebasByEquipoId(); ?>
                    <?php while($pru = $pruebas->fetch_object()) : ?>
                        <tr>
                            <td><?=$pru->fecha_prueba;?></td>
                            <td><?=$pru->nombre;?></td>
                            <td><?=$pru->abreviacion;?></td>
                            <td>
                                <?php     
                                    if($pru->tipo_prueba == 1){
                                        echo "<img src=".base_url."img/electrica.png>";
                                    } else {
                                        echo "<img src=".base_url."img/aceite.png>";
                                    }
                                ?>   
                            </td>
                            <td>
                                <?php     
                                    if($pru->resultado == 1){
                                        echo "<img src=".base_url."img/critico.png>";
                                    } elseif ($pru->resultado == 2) {
                                        echo "<img src=".base_url."img/cuestionable.png>";
                                    } else {
                                        echo "<img src=".base_url."img/aceptable.png>";
                                    }
                                ?>           
                            </td>
                            <td><?=$pru->recomend;?></td>   
                            <?php if(empty($pru->archivo) || $pru->archivo==''){?>
                            <td>
                                Sin Archivo
                            </td>
                            <?php }else{?>
                            <td>
                                <a href="<?=base_url?>uploads/pruebas/<?=$pru->archivo;?>" class="boton boton-orange" download="Reporte Prueba <?=$pru->nombre;?>">
                                    <?=$pru->archivo;?>
                                </a>
                            </td>
                            <?php }?>
                            <td>
                                <a href="<?=base_url?>prueba/editar&id=<?=$pru->id?>&equid=<?=$equi->id?>&proid=<?=$proid?>" class="boton boton-blue">Editar</a>
                                <a href="#" onclick="preguntar(<?=$pru->id?>)" class="boton boton-red">Eliminar</a>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                </table>
            <div class="data-table">
        </div>

<?php endif; ?>


<!-- **********************  SECCION PARA CLIENTES  ****************************** -->

<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "cliente") : ?>
    <?php   
        if($_GET['proid']){
            $proid = $_GET['proid'];
        }
    ?>

            <div id="portada">
                <h2>Datos del equipo:</h2>
                    <?php if(isset($equi)): ?>
                        <?php if($equi->imagen !=null): ?>
                            <img src="<?=base_url?>uploads/images/<?=$equi->imagen?>" class="thumb"/>
                        <?php else: ?>
                        <img src="<?=base_url?>uploads/images/equipo_generico.jpg" />  
                    <?php endif; ?>
            </div>
            <div id="infohead">
                <h1><?=$equi->nombre." - ".$equi->serie?></h1> 
                <h3>Marca: <?=$equi->marca." - Modelo: ".$equi->modelo?></h3>
                <p><?=$equi->descripcion?></p>
                    <?php $proyecto = Utilities::showCurrentProid(); ?>
                        <h3>Proyecto al que pertenece: 
                           <?=$proyecto->nombre?>
                        </h3>   
         
            </div>
                <?php else: ?>
                    <h1>El Equipo no existe</h1>
                <?php endif; ?>

        <?php
            if($_GET['id']){
                $id = $_GET['id'];
            }
        ?>
        <div class="content-hijo">
            <h1>Gestion de Pruebas Eléctricas y al Aceite</h1>

            <table border="1">
                <thead>
                    <tr>
                        <th>FECHA</th>
                        <th>NOMBRE</th>
                        <th>ABREV</th>
                        <th>TIPO</th>
                        <th>RESULTADO</th>
                        <th>RECOMENDACION</th>
                        <th>ARCHIVO</th>
                    </tr>
                </thead>
                <?php $pruebas = Utilities::getPruebasByEquipoId(); ?>
                <?php while($pru = $pruebas->fetch_object()) : ?>
                    <tr>
                        <td><?=$pru->fecha_prueba;?></td>
                        <td><?=$pru->nombre;?></td>
                        <td><?=$pru->abreviacion;?></td>
                        <td>
                            <?php     
                                if($pru->tipo_prueba == 1){
                                    echo "<img src=".base_url."img/electrica.png>";
                                } else {
                                    echo "<img src=".base_url."img/aceite.png>";
                                }
                            ?>   
                        </td>
                        <td>
                            <?php     
                                if($pru->resultado == 1){
                                    echo "<img src=".base_url."img/critico.png>";
                                } elseif ($pru->resultado == 2) {
                                    echo "<img src=".base_url."img/cuestionable.png>";
                                } else {
                                    echo "<img src=".base_url."img/aceptable.png>";
                                }
                            ?>           
                        </td>
                        <td><?=$pru->recomend;?></td>
                        <?php if(empty($pru->archivo) || $pru->archivo==''){?>
                        <td>
                            <a href="#" class="boton boton-orange">
                                Sin Archivo
                            </a>
                        </td>
                        <?php }else{?>
                        <td>
                            <a href="<?=base_url?>uploads/pruebas/<?=$pru->archivo;?>" class="boton boton-orange" download="Reporte Prueba <?=$pru->nombre;?>">
                                <?=$pru->archivo;?>
                            </a>
                        </td>
                        <?php }?>

                    </tr>
                <?php endwhile; ?>
            </table>
        </div>

<?php endif; ?>


<script type="text/javascript">
		

		function preguntar(id){
			var confirm = alertify.confirm('¿Está seguro que desea borrar esta PRUEBA?',' Esto no se puede revertir. ¿Está Seguro?"',null,null).set('labels', {ok:'borrar', cancel:'NO BORRAR'}); 

			//callbak al pulsar botón positivo
			confirm.set('onok', function(){
                window.location.href = "<?=base_url?>prueba/eliminar&id="+id+"&equid=<?=$id?>&proid=<?=$proid?>";
			    alertify.success('Se ha borrado el Proyecto');
			});
			//callbak al pulsar botón negativo
			confirm.set('oncancel', function(){ 
			    alertify.error('No se ha borrado nada');
			})	

		}
</script>