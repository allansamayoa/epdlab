<?php if($_GET['proid']){
        $proid = $_GET['proid'];
        }
?>
<div class="ProTitulo">
<?php if(isset($edit) && isset($equi) && is_object($equi)) : ?>
    <h1>EDITANDO EQUIPO: <?=$equi->nombre;?></h1>
    <?php $url_action = base_url."equipo/save&proid=".$proid."&id=".$equi->id;?>
<?php else: ?>
    <?php $proyecto = Utilities::showCurrentProyectEqui(); ?>
    <h2>AGREGANDO NUEVO EQUIPO A: <?=$proyecto->nombre;?></h2>

    <?php $url_action = base_url."equipo/save&proid=".$proid;?>
<?php endif; ?>



<?php $proyecto = Utilities::showCurrentProyectEqui(); ?>
<h3><?=$proyecto->nombre?></h3>
</div>
<!--
private $id;
            private $proyecto_id;
        private $nombre;
        private $marca;
        private $modelo;
        private $serie;
        private $fabricante;
        private $descripcion;
    private $fecha_crea;
    private $imagen;
-->

<form action="<?=$url_action?>" method="POST" class="formulario" enctype="multipart/form-data">

    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" value="<?=isset($equi) && is_object($equi) ? $equi->nombre : '';?>" />

    <label for="modelo">Modelo</label>
    <input type="text" name="modelo" value="<?=isset($equi) && is_object($equi) ? $equi->modelo : '';?>" />

    <label for="serie">Serie</label>
    <input type="text" name="serie" value="<?=isset($equi) && is_object($equi) ? $equi->serie : '';?>" />

    <label for="fabricante">Fabricante</label>
    <input type="text" name="fabricante" value="<?=isset($equi) && is_object($equi) ? $equi->fabricante : '';?>" />

    <label for="descripcion">Descripción</label>
    <textarea rows="4" cols="50" name="descripcion" ><?=isset($equi) && is_object($equi) ? $equi->descripcion : '';?></textarea>
   
    <label for="imagen">Imagen</label>
    <?php if(isset($equi) && is_object($equi) && !empty($equi->imagen)) : ?>
        <img src="<?=base_url?>uploads/images/<?=$equi->imagen?>" class="thumb" />
    <?php endif; ?>
    <br>
    <input type="file" name="imagen" />
    <br>
    <br>
    <hr>

    <?php if(isset($edit) && isset($equi) && is_object($equi)) : ?>
        <input type="submit" value="Actualizar Equipo"  />
    <?php else: ?>
        <input type="submit" value="Agregar Equipo"  />
    <?php endif; ?>

</form>



