<h1>Gestion de Informes</h1>
        <?php   
            if($_GET['id']){
                $proid = $_GET['id'];
            }
        ?>
<a href="<?=base_url?>informe/crear&proid=<?=$proid?>" class="boton boton-peque">
Crear un nuevo informe para este proyecto.
</a>

<div class="data-table">
    <table border="1">
        <tr>
            <th>EQUIPO</th>
            <th>SERIE</th>
            <th>FECHA INF</th>
            <th>RESULT ELEC</th>
            <th>RECOM ELEC</th>
            <th>RESULT ACEITE</th>
            <th>RECOM ACEITE</th>
            <th>ARCHIVO</th>
            <th>ACCION</th>
        </tr>
        <?php while($infor = $informes->fetch_object()) : ?>

            <tr>
                <td><?=$infor->equipo_id;?></td>
                <td><?=$infor->serie;?></td>
                <td><?=$infor->fecha_informe;?></td>
                <td>
                    <?php     
                        if($infor->result_electricas == 1){
                            echo "<img src=".base_url."img/critico.png>";
                        } elseif ($infor->result_electricas == 2) {
                            echo "<img src=".base_url."img/cuestionable.png>";
                        } elseif ($infor->result_electricas == 3) {
                            echo "<img src=".base_url."img/aceptable.png>";
                        }else{
                            echo "N/A";
                        }
                    ?>
                </td>
                <td><?=$infor->recom_electricas;?></td>
                <td>
                <?php     
                        if($infor->result_aceite == 1){
                            echo "<img src=".base_url."img/critico.png>";
                        } elseif ($infor->result_aceite == 2) {
                            echo "<img src=".base_url."img/cuestionable.png>";
                        } elseif ($infor->result_aceite == 3) {
                            echo "<img src=".base_url."img/aceptable.png>";
                        }else{
                            echo "N/A";
                        }
                    ?>
                </td>
                <td><?=$infor->recom_aceite;?></td>
                <td><?=$infor->informe;?></td>
                <td>
                    <a href="<?=base_url?>informe/editar&id=<?=$infor->id?>" class="action action-blue" >Editar</a>
                    <a href="#" onclick="preguntar(<?=$infor->id?>)" class="action action-red">Eliminar</a>
                </td>
            </tr>
        <?php endwhile; ?>

    </table>
</div>


<script type="text/javascript">
		

		function preguntar(id){
			var confirm = alertify.confirm('¿Está seguro que desea borrar el INFORME?',' Esto no se puede revertir. ¿Está Seguro?"',null,null).set('labels', {ok:'Confirmar BORRADO', cancel:'NO BORRAR'}); 

			//callbak al pulsar botón positivo
			confirm.set('onok', function(){
                window.location.href = "<?=base_url?>informe/eliminar&id="+id;
			    alertify.success('Se ha borrado el Proyecto');
			});
			//callbak al pulsar botón negativo
			confirm.set('oncancel', function(){ 
			    alertify.error('No se ha borrado nada');
			})	

		}
</script>


