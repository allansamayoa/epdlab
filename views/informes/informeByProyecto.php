<!-- **********************  SECCION PARA ADMINS  ****************************** -->

<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "admin") : ?>
    <h2>Gestion de Informes</h2>


    <?php if($_GET['id']){
    $id = $_GET['id'];
    }
    ?>

    <?php $pro = Utilities::showCurrentProyect(); ?>
    <p><a href="<?=base_url?>proyecto/show&id=<?=$pro->id?>">Proyecto: <?=$pro->nombre?><a></p>
    


    <?php if($informes->num_rows != 0) : ?>
    <a href="<?=base_url?>informe/crear&proid=<?=$id?>" class="boton boton-peque">Agregar Informe al Proyecto</a>
        <div class="data-table">
            <table border="1">
            <thead>
                <tr>
                    <th>EQUIPO</th>
                    <th>SERIE</th>
                    <th>FECHA INF</th>
                    <th><img src="<?=base_url;?>img/electrica.png"> <br>RESULTADOS DE PRUEBAS ELECTRICAS </th>
                    <th>RECOMENDACIONES ELECTRICAS</th>
                    <th><img src="<?=base_url;?>img/aceite.png"> <br>RESULTADOS DE PRUEBAS AL ACEITE</th>
                    <th>RECOMENDACIONES AL ACEITE</th>
                    <th>ARCHIVO PE</th>
                    <th>ARCHIVO PA</th>
                    <th>ACCION</th>
                </tr>
            </thead>
            <?php while($infor = $informes->fetch_object()) : ?>
            <?php //var_dump($infor); die(); ?>
                    <?php
                        $equide = $infor->equipo_id;
                        $equi = Utilities::showSelectedEquipo($equide);
                    ?>
                    <tr>
                        <td>
                        <a href="<?=base_url?>equipo/show&id=<?=$equi->id;?>&proid=<?=$pro->id?>" ><?=$equi->nombre;?></a>          
                        </td>
                        <td><?=$infor->serie;?></td>
                        <td><?=$fecha= $infor->fecha_informe;
                                $fecha = date('d-m-Y H:i:s');
                                //echo $fecha
                                ?></td>
                        <td>
                            <?php     
                                if($infor->result_electricas == 1){
                                    echo "<img src=".base_url."img/critico.png>";
                                } elseif ($infor->result_electricas == 2) {
                                    echo "<img src=".base_url."img/cuestionable.png>";
                                } elseif ($infor->result_electricas == 3) {
                                    echo "<img src=".base_url."img/aceptable.png>";
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td><?=$infor->recom_electricas;?></td>
                        <td>
                        <?php     
                                if($infor->result_aceite == 1){
                                    echo "<img src=".base_url."img/critico.png>";
                                } elseif ($infor->result_aceite == 2) {
                                    echo "<img src=".base_url."img/cuestionable.png>";
                                } elseif ($infor->result_aceite == 3) {
                                    echo "<img src=".base_url."img/aceptable.png>";
                                }else{
                                    echo "N/A";
                                }
                            ?>
                        </td>
                        <td><?=$infor->recom_aceite;?></td>
                        <td>
                            <?php if(!empty($infor->archivo)){?>
                            <a href="<?=base_url?>uploads/informes/<?=$infor->archivo;?>" download="<?=$infor->archivo;?>" class="boton boton-orange"><?=$infor->archivo;?></a>
                            <?php }else{echo 'Sin Informe';}?>
                        </td>
                        <td>
                            <?php if(!empty($infor->archivo_pa)){?>
                            <a href="<?=base_url?>uploads/informes/<?=$infor->archivo_pa;?>" download="<?=$infor->archivo_pa;?>" class="boton boton-orange"><?=$infor->archivo_pa;?></a>
                            <?php }else{echo 'Sin Informe';}?>
                        </td>
                        <td>
                            <a href="<?=base_url?>informe/editar&id=<?=$infor->id?>&proid=<?=$pro->id?>&equid=<?=$equi->id;?>" class="action action-blue" >Editar</a>
                            <a href="#" onclick="preguntarinf(<?=$infor->id?>)" class="action action-red">Eliminar</a>
                        </td>
                    </tr>
                <?php endwhile; ?>
            </table>
        </div>
    <?php else : ?>                       
        <h3>"Aún no hay INFORMES en este Proyecto"</h3>
        <p>Agregue informes al proyecto desde aquí: <a href="<?=base_url?>informe/crear&proid=<?=$id?>" class="boton boton-orange">Agregar Informe</a>
        </p>
    <?php endif; ?>

<?php endif; ?>



<!-- **********************  SECCION PARA CLIENTES  ****************************** -->

<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "cliente") : ?>

<h2>Ver informes de mi proyecto:</h2>


<?php if($_GET['id']){
$id = $_GET['id'];
}
?>

<?php $pro = Utilities::showCurrentProyect(); ?>
<h1><?=$pro->nombre?></h1>

<?php if($informes->num_rows != 0) : ?>
        <table border="1">
            <thead>
                <tr>
                    <th>EQUIPO</th>
                    <th>SERIE</th>
                    <th>FECHA INF</th>
                    <th>RESULTADOS DE PRUEBAS ELECTRICAS <img src="<?=base_url;?>img/electrica.png"></th>
                    <th>RECOMENDACIONES ELECTRICAS</th>
                    <th>RESULTADOS DE PRUEBAS AL ACEITE<img src="<?=base_url;?>img/aceite.png"></th>
                    <th>RECOMENDACIONES AL ACEITE</th>
                    <th>ARCHIVO </th>
                </tr>
            </thead>
            <?php while($infor = $informes->fetch_object()) : ?>
            <?php //var_dump($infor); die(); ?>
                    <?php
                        $equide = $infor->equipo_id;
                        $equi = Utilities::showSelectedEquipo($equide);
                    ?>
                    <tr>
                        <td>
                        <a href="<?=base_url?>equipo/show&id=<?=$equi->id;?>&proid=<?=$pro->id?>" ><?=$equi->nombre;?></a>          
                        </td>
                        <td><?=$infor->serie;?></td>
                        <td><?=$infor->fecha_informe;?></td>
                        <td>
                            <?php     
                                if($infor->result_electricas == 1){
                                    echo "<img src=".base_url."img/critico.png>";
                                } elseif ($infor->result_electricas == 2) {
                                    echo "<img src=".base_url."img/cuestionable.png>";
                                } else {
                                    echo "<img src=".base_url."img/aceptable.png>";
                                }
                            ?>
                        </td>
                        <td><?=$infor->recom_electricas;?></td>
                        <td>
                        <?php     
                                if($infor->result_aceite == 1){
                                    echo "<img src=".base_url."img/critico.png>";
                                } elseif ($infor->result_aceite == 2) {
                                    echo "<img src=".base_url."img/cuestionable.png>";
                                } else {
                                    echo "<img src=".base_url."img/aceptable.png>";
                                }
                            ?>
                        </td>
                        <td><?=$infor->recom_aceite;?></td>
                        <td><a href="<?=base_url?>uploads/informes/<?=$infor->archivo;?>" download="<?=$infor->archivo;?>" class="boton boton-orange"><?=$infor->archivo;?></a></td>
                    </tr>
                <?php endwhile; ?>
            </table>
    <?php else : ?>                       
        <h3>"Aún no hay INFORMES en este Proyecto"</h3>
        <p>Permanezca pendiente en cuanto el equipo de Energia PD suba el informe.</p>
    <?php endif; ?>


<?php endif; ?>


<script type="text/javascript">
		

		function preguntarinf(id){
			var confirm = alertify.confirm('¿Está seguro que desea borrar este INFORME?',' Esto no se puede revertir. ¿Está Seguro?"',null,null).set('labels', {ok:'borrar', cancel:'NO BORRAR'}); 

			//callbak al pulsar botón positivo
			confirm.set('onok', function(){
                window.location.href = "<?=base_url?>informe/eliminar&id="+id+"&proid=<?=$pro->id?>";
			    alertify.success('Se ha borrado el Proyecto');
			});
			//callbak al pulsar botón negativo
			confirm.set('oncancel', function(){ 
			    alertify.error('No se ha borrado nada');
			})	

		}
</script>