<div class="ProTitulo2">
	<?php if(isset($edit) ) : ?>
		<h2>Editar Informe del:</h2> 
			<?php 
				if($_GET['proid']){
					$id = $_GET['proid'];
				}

				if($_GET['id']){
					$infor_id = $_GET['id'];
				}
			?>
			<?php $pro = Utilities::showCurrentProyectProid(); ?>
			<h1>Proyecto: <?=$pro->nombre?></h1>   
			<p>Edite el informe actual</p>
			<?php $url_action = base_url."informe/save&id=".$infor_id."&proid=".$id?>
	<?php else: ?>
		<h2>Agregar Informe a:</h2> 
			<?php 
				if($_GET['proid']){
					$id = $_GET['proid'];
				}
			?>
			<?php $pro = Utilities::showCurrentProyectProid(); ?>
			<h1>Proyecto: <?=$pro->nombre?></h1>   
			<p>Registre un nuevo informe de pruebas electricas y de laboratorio</p>
			<?php $url_action = base_url."informe/save&proid=".$id;?>
	<?php endif; ?>
	</div>
		<!-- Mostrar Errores -->


			<!-- Formulario -->
		<div id="formulario-head">

				<?php 
					if($_GET['proid']){
					$id = $_GET['proid'];
					}
				?>
				<label for="proyecto"><h3>Proyecto</h3></label>
				<?php $proyecto = Utilities::showCurrentProyectProid(); ?>
				<h3><?=$proyecto->nombre?></h3>
				<?php
					$columns='formulariocol_pea';
					if (isset($informe) && is_object($informe)) {
						$statusE=$informe->result_electricas; //estatus de prueba Electrica
						$statusA=$informe->result_aceite; //estatus de prueba aceite
						$isPA='';
						$isPE='';
						if (empty($informe->recom_aceite)) {
							$isPA='form-inactive';
						}
						if (empty($informe->recom_electricas)) {
							$isPE='form-inactive';
						}
						if (!empty($informe->recom_aceite) && !empty($informe->recom_electricas)) {
							$columns='formulario2col';
						}
					}
				?>
			<?php if(isset($edit) ) : ?>
				<h2><label for="equipo">Equipo Evaluado</label></h2>
				<p>Seleccione el Equipo al que se le registra este informe</p>
				<?php $equipos = Utilities::showEquiposInfor(); ?>

				<select name="equipo">
					<?php while($equi = $equipos->fetch_object()) : ?>
						<option value="<?=$equi->id?>" <?=isset($informe) && is_object($informe) && $equi->id == $informe->equipo_id ? 'selected' : '';?>>
							<?=$equi->nombre?>
						</option>
					<?php endwhile; ?>
				</select>

			<?php else: ?>

				<h2><label for="equipo">Equipo Evaluado</label></h2>
				<p>Seleccione el Equipo al que se le registra este informe</p>
				<?php $equipos = Utilities::showEquiposProid(); 
					if(isset($_POST['equipos'])){
						$equipoS=$_POST['equipos'];
					}else{
						$equipoS='';
					}
				?>
				<form method="POST" action="<?=base_url?>informe/crear&proid=<?=$proyecto->id?>">	
					<select name="equipos" onchange="resetData()">
						<?php while($equi = $equipos->fetch_object()) : ?>
							<option value="<?=$equi->id?>" <?=$equi->id==$equipoS? 'selected': '';?>>
								<?=$equi->nombre?>
							</option>
						<?php endwhile; ?>
					</select>
					<input type="submit" name="send" value="Generar">
				</form>
			<?php endif; ?>

		<?php //Formulario de Informes;
			if (isset($_POST['equipos'])){
				if (!isset($informe)){
					$status = new Informe();
					$status->setProyect_id($pro->id);
					$status->setEquipo_id($_POST['equipos']);
					$reslt = $status->getStatus()->fetch_assoc();
					$statusE=$reslt['electrica']; //estatus de prueba Electrica
					$statusA=$reslt['aceite']; //estatus de prueba aceite
					$isPE='';
					$isPA='form-inactive';
					if ($statusE=='0' && $statusA!='0') {
						$checkedE='';
						$checkedA='checked';
						$disabledE='disabled';
						$disabledA='disabled';
					}elseif ($statusE!='0' && $statusA=='0') {
						$checkedE='checked';
						$checkedA='';
						$disabledE='disabled';
						$disabledA='disabled';
					}elseif ($statusE!='0' && $statusA!='0') {
						$checkedE='checked';
						$checkedA='';
						$disabledE='disabled';
						$disabledA='';
					}
				}
			if ($statusE=='0' && $statusA=='0') {
		?>
		<p>Debe registrar pruebas para este equipo antes de generar un informe</p>
		<?php }else{?>
		<div id="form-div">
			<form action="<?=$url_action?>" method="POST" enctype="multipart/form-data">
					<input type="hidden" name="equipo" value="<?=$equipoS?>">

					<h2><label for="fecha_informe">Fecha del informe</label></h2>
					<p>Seleccione la fecha en la que se REGISTRA este informe</p>
					<input type="date" name="fecha_informe" value="<?=isset($informe) && is_object($informe) ? $informe->fecha_informe : date('Y-m-d') ;?>" />
				<?php if(!isset($edit) ) : ?>
					<h2><label for="fecha_informe">Tipo de Pruebas</label></h2>
					<p>Seleccione la el tipo de prueba a cargar</p>
					<div class="formulario2colclean">
						<label class="chkcontainer">Prueba Eléctrica
							<input type="checkbox" id="chk_PE" onclick="check_test ('form-elc','file_PE')" <?=$checkedE.' '.$disabledE ?>>
							<span class="checkmark"></span>
						</label> 
						<label class="chkcontainer">Prueba Aceite
							<input type="checkbox" id="chk_PA" onclick="check_test ('form-act','file_PA')" <?=$checkedA.' '.$disabledA ?>>
							<span class="checkmark"></span>
						</label> 
					</div>
				<?php endif; ?>

				<div id="<?=$columns;?>">
					<div id="form-elc" class="<?=$isPE;?>">
						<img src="<?=base_url?>img/electrica.png">
						<h2><label for="result_electricas">Resultados Pruebas Electricas</label></h2>
						<div>
							<input type="radio" class="input-hidden" id="elect3" name="result_electricas" value="3" <?=$statusE == 3 ? 'checked' : 'disabled';?> >
							<label for="elect3" class="label-check" title="Aceptable">
							  
							  <img 
							    src="<?=base_url?>img/aceptable.png" 
							  />
							</label >
							<input type="radio" name="result_electricas" class="input-hidden" id="elect2" value="2" <?=$statusE == 2 ? 'checked' : 'disabled';?> > 
							<label for="elect2" class="label-check" title="Custionable">
							  <img 
							    src="<?=base_url?>img/cuestionable.png" 
							  />
							</label>
							<input type="radio" name="result_electricas" class="input-hidden" id="elect1" value="1" <?=$statusE == 1 ? 'checked' : 'disabled';?> > 
							<label for="elect1" class="label-check" title="Crítico">
							  <img 
							    src="<?=base_url?>img/critico.png" 
							  />
							</label>
						</div>

						<label for="recom_electricas">Recomendaciones Eléctricas</label>
						<textarea name="recom_electricas" rows="10" cols="30" placeholder=" Ingrese sus recomendaciones" ><?=isset($informe) && is_object($informe) ? $informe->recom_electricas : '';?></textarea>
					</div>
					<?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'recom_elec') : ''; ?>

					<div id="form-act" class="<?=$isPA;?>">   
						<img src="<?=base_url?>img/aceite.png">
						<h2><label for="result_aceite">Resultados Pruebas al Aceite</label></h2>
						<div>
								<input type="radio" name="result_aceite" class="input-hidden" id="aceite3" value="3" <?=$statusA == 3 ? 'checked' : 'disabled';?> >
								<label for="aceite3" class="label-check" title="Aceptable">
								  <img 
								    src="<?=base_url?>img/aceptable.png" 
								  />
								</label >

								<input type="radio" name="result_aceite" class="input-hidden" id="aceite2" value="2" <?=$statusA == 2 ? 'checked' : 'disabled';?> >
								<label for="aceite2" class="label-check" title="Custionable">
								  <img 
								    src="<?=base_url?>img/cuestionable.png" 
								  />
								</label >

								<input type="radio" name="result_aceite" class="input-hidden" id="aceite1" value="1" <?=$statusA == 1 ? 'checked' : 'disabled';?> >
								<label for="aceite1" class="label-check" title="Crítico">
								  <img 
								    src="<?=base_url?>img/critico.png" 
								  />
								</label >

						</div>

						<label for="recom_aceite">Recomendaciones al Aceite</label>
						<textarea name="recom_aceite" rows="10" cols="30" placeholder=" Ingrese sus recomendaciones"><?=isset($informe) && is_object($informe) ? $informe->recom_aceite : '';?></textarea>
					</div>


				</div> 
				<div class="<?php if(empty($isPE) && empty($isPA)){echo 'formulario2col';}else{echo '';}?>">
				<?php if(isset($informe) && is_object($informe) && !empty($informe->archivo)): ?>
					<div id="file_PE" class="<?=$isPE;?>">
						<img src="<?=base_url?>img/pdf_icon.png" class="icon">
						<h4>Archivo Actual: <a class="link_file" href="<?=base_url?>uploads/informes/<?=$informe->archivo?>" download="<?=$informe->archivo;?>">Descargar Archivo<!--?=$informe->archivo?--></a></h4>
						<h3><label for="archivo">Cambiar Archivo PDF de Informe</label><h3>
						<h3><input type="file" name="archivo" accept="application/pdf" ><h3>
					</div>
					<div id="file_PA" class="<?=$isPA;?>">
						<img src="<?=base_url?>img/pdf_icon.png" class="icon">
						<h4>Archivo Actual: <a class="link_file" href="<?=base_url?>uploads/informes/<?=$informe->archivo_pa?>" download="<?=$informe->archivo_pa;?>">Descargar Archivo <!--?=$informe->archivo_pa?--></a></h4>
						<h3><label for="archivo">Cambiar Archivo PDF de Informe</label><h3>
						<h3><input type="file" name="archivo_pa" accept="application/pdf" ><h3>
					</div>

					<?php else: ?>
					<div id="file_PE">
						<img src="<?=base_url?>img/pdf_icon.png" class="icon">
						<h3><label for="archivo">Subir Archivo PDF de Informe Eléctrica</label><h3>
						<h3><input type="file" name="archivo" accept="application/pdf" ><h3>
					</div>
					<div id="file_PA" class="form-inactive">
						<img src="<?=base_url?>img/pdf_icon.png" class="icon">
						<h3><label for="archivo">Subir Archivo PDF de Informe Aceite</label><h3>
						<h3><input type="file" name="archivo_pa" accept="application/pdf" ><h3>
					</div>

				<?php endif; ?>
				</div>
				<?php if(isset($edit) ) : ?>
					<input type="submit" name="submit" value="Actualizar Informe" />
				<?php else: ?>
					<input type="submit" name="submit" value="Registrar Informe" />
				<?php endif; ?>
			</form>
		</div>
		<?php //Formulario de Informes;
				}
			}
		?>
	</div>  
<script type="text/javascript">
	function resetData(){
		document.getElementById('form-div').style.display = 'none'
	}
</script>
	