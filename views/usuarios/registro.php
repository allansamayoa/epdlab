<?php if(isset($edit)){?>

    <h1>Edite el perfil de  Usuario Aquí</h1>
    <?php
        $url_action = base_url."usuario/save&id=".$id;
    ?>
<?php }else{?>

    <h1>Registre un Usuario Aquí</h1>
    <?php
        $url_action = base_url."usuario/save";
    ?>
<?php }?>



<?php if(isset($_SESSION['register']) && $_SESSION['register'] == 'complete'): ?>
    <strong class="alerta alerta-exito">Registro Completado Correctamente</strong>
<?php elseif(isset($_SESSION['register']) && $_SESSION['register'] == 'failed'): ?>
    <strong class="alerta alerta-error">Registro Fallido, introduce bien los datos.</strong>
<?php endif; ?>
<?php Utilities::deleteSession('register'); ?>

<div id="formulario2colclean">
    <form action="<?=$url_action?>" method="POST">

        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" value="<?=isset($usuarios) && is_object($usuarios)? $usuarios->nombre : '';?>" />
        
        <label for="apellidos">Apellidos</label>
        <input type="text" name="apellidos" value="<?=isset($usuarios) && is_object($usuarios)? $usuarios->apellidos : '';?>" />
        
        <label for="email">Email</label>
        <input type="email" name="email" value="<?=isset($usuarios) && is_object($usuarios)? $usuarios->email : '';?>" />

        <label for="password">Contraseña</label>
        <input type="password" name="password" placeholder="<?=isset($usuarios) && is_object($usuarios)? 'Omitir si desea conservar la anterior' : '';?>" />
                
        
    </div>   
    <label for="rol">Permisos</label>
        <select name="rol">
            <option value="basic" <?=isset($usuarios) && is_object($usuarios) && $usuarios->rol=='basic'? 'selected':'' ?> >Básico</option>
            <option value="admin" <?=isset($usuarios) && is_object($usuarios) && $usuarios->rol=='admin'? 'selected':'' ?> >Administrador</option>
        </select> 
        <?php if(isset($edit)){?>
            <input type="submit" name="submit" value="Actualizar" />
        <?php }else{?>
            <input type="submit" name="submit" value="Registrar" />
        <?php }?>

    </form>

    

