<div class="ProTitulo">
        <h1>Gestionar Usuarios</h1>
        <a href="<?=base_url?>usuario/registro" class="boton boton-peque">Agregar Nuevo Usuario</a>
    </div>
    <div class="content-hijo">

        <div class="data-table">
            <table border="1">
                <tr>
                    <th>NOMBRE</th>
                    <th>APELLIDOS</th>

                    <th>EMAIL</th>

                    <th>FECHA REGISTRO</th>
                    <th>ACCIONES</th>
                </tr>
                <?php while($cli = $usuarios->fetch_object()) : ?>
                    <tr>
                        <td><?=$cli->nombre;?></td>
                        <td><?=$cli->apellidos;?></td>

                        <td><?=$cli->email;?></td>

                        <td><?=$cli->fecha_reg;?></td>
                        <td>
                            <a href="<?=base_url?>usuario/editar&id=<?=$cli->id?>" class="action action-blue" >Editar</a>
                            <a href="#" onclick="preguntar(<?=$cli->id?>)" class="action action-red">Eliminar</a>

                        </td>
                    </tr>
                <?php endwhile; ?>

            </table>
        </div>
    </div>
    <script type="text/javascript">
        

        function preguntar(id){
            var confirm = alertify.confirm('¿Está seguro que desea borrar el USUARIO?',' Esto no se puede revertir.  ¿Está Seguro?"',null,null).set('labels', {ok:'Confirmar BORRADO', cancel:'NO BORRAR'}); 

            //callbak al pulsar botón positivo
            confirm.set('onok', function(){
                window.location.href = "<?=base_url?>usuario/eliminar&id="+id;
                alertify.success('Se ha borrado el Proyecto');
            });
            //callbak al pulsar botón negativo
            confirm.set('oncancel', function(){ 
                alertify.error('No se ha borrado nada');
            })  

        }
    </script>
