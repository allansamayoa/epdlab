<?php if(!isset($_SESSION['identity'])) : ?>
<div id="login">
        
        
        <img src="<?=base_url?>img/identify.png"/>
        <h2>IDENTIFÍQUESE</h2>
        <p>
            Si tiene un usuario y contraseña para revisar 
            un resumen de los resultados de las pruebas 
            de sus equipos, ingréselos aquí.
        </p>
            <div class="formulario">
            <form action="<?=base_url?>usuario/login" method="POST">
                <label for="email" >Email</label>
                <input type="email" placeholder="Ingrese su correo electrónico" 
                name="email" />

                <label for="password">Contraseña</label>
                <input type="password" name="password" placeholder="Ingrese su clave"/>
                <br>

                <!-- ALERTA DE ERROR INGRESO -->
                <?php if(isset($_SESSION['error_login'])): ?>
                <br>
                <hr>
                <br>
                    <strong class="alerta alerta-error">No se ha podido ingresas correctamente</strong><br>
                    <br>
                    <p>Asegurese está colocando el password correcto, que está usando un Mail 
                       Válido para ingresar y que selecciona su perfil correctamente</p>
                <br>
                <hr>
                <?php endif; ?>
                <?php Utilities::deleteSession('error_login'); ?>

                <button type="submit" class="boton">Ingresar</button>
            </form>
            </div>




</div>
<?php else: ?>
    <?php    require_once 'instrucciones.php'; ?>
<?php endif; ?>