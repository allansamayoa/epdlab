    <div class="ProTitulo">
    <?php if(isset($edit) && isset($pro) && is_object($pro)) : ?>
        <h1>Editar Proyecto <?=$pro->nombre;?></h1>
        <p>Edite el proyecto actual</p>
        <?php $url_action = base_url."proyecto/save&id=".$pro->id;?>
    <?php else: ?>
        <h1>Crear Nuevo Proyecto</h1>
        <p>No olvide asignar el CLIENTE correspondiente al PROYECTO que va a crear.</p>
        <?php $url_action = base_url."proyecto/save";?>
    <?php endif; ?>
    </div>
    <div class="content-hijo">


        <form action="<?=$url_action?>" method="POST" class="formulario">

            <label for="nombre">Nombre del Proyecto</label>
            <input type="text" name="nombre" value="<?=isset($pro) && is_object($pro) ? $pro->nombre : '';?>" />

            <label for="ciudad">Ciudad en la que descansa el proyecto</label>
            <input type="text" name="ciudad" value="<?=isset($pro) && is_object($pro) ? $pro->ciudad : '';?>" />

            <label for="descripcion">Descripción</label>
            <textarea rows="4" cols="50" name="descripcion"><?=isset($pro) && is_object($pro) ? $pro->descripcion : '';?></textarea>
        
            <label for="cliente">Asigne a cliente:</label>
            <?php $clientes = Utilities::showClientes(); ?>
                <select name="cliente">
                    <?php while($cli = $clientes->fetch_object()): ?>
                        <option value="<?=$cli->id?>" <?=isset($pro) && is_object($pro) && $cli->id == $pro->cliente_id ? 'selected' : '';?>> 
                            <?=$cli->nombre?>
                        </option>
                    <?php endwhile; ?>
                </select>

            <input type="submit" value="Guardar Proyecto" />
        </form>
        </div>



