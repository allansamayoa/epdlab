<div class="ProTitulo">

    <h1>Ver Proyectos</h1>
</div>
<br>
<hr>
<br>
<div class="content-hijo">

<!-- ALERTA DE CREADO -->
<?php if(isset($_SESSION['proyecto']) && $_SESSION['proyecto'] == 'complete'): ?>
    <strong class="alerta alerta-exito">Se ha CREADO el proyecto correctamente.</strong>
<?php elseif(isset($_SESSION['proyecto']) && $_SESSION['proyecto'] == 'failed'): ?>
    <strong class="alerta alerta-error">No se ha logrado CREAR el proyecto.</strong>
<?php endif; ?>
<?php Utilities::deleteSession('proyecto'); ?>

<!-- ALERTA DE BORRADO -->
<?php if(isset($_SESSION['delete']) && $_SESSION['delete'] == 'complete'): ?>
    <strong class="alerta alerta-exito">Se ha borrado el proyecto correctamente de la base de datos.</strong>
<?php elseif(isset($_SESSION['delete']) && $_SESSION['delete'] == 'failed'): ?>
    <strong class="alerta alerta-error">No se ha logrado borrar el proyecto.</strong>
<?php endif; ?>
<?php Utilities::deleteSession('delete'); ?>
<div class="data-table">
    <table border="1">
        <tr>
            <th>NOMBRE</th>
            <th>CIUDAD</th>
            <th>CLIENTE</th>
            <th>CONTACTO</th>
            <th>DESCRIPCION</th>
            <th>FECHA</th>
            <th>OPCIONES</th>
        </tr>
        <?php while($pro = $proyectos->fetch_object()) : ?>
            <tr>
                <td>
                    <a href="<?=base_url?>proyecto/show&id=<?=$pro->id?>"><?=$pro->nombre;?></a>
                </td>
                <td><?=$pro->ciudad;?></td>
                <?php $clientes = Utilities::showClientes();?>
                <?php $clie = $clientes->fetch_object() ; ?>
                <?php
                $clide = $pro->cliente_id;
                $clien = Utilities::showSelectedCliente($clide);
                ?>
                <td><?=$clien->empresa;?></td>
                <td><?=$clien->nombre;?></td>
                <td><?=$pro->descripcion;?></td>
                <td><?=$pro->fecha_crea;?></td>
                <td><a href="<?=base_url?>proyecto/editar&id=<?=$pro->id?>" class="boton boton-blue">Editar</a>
                <a href="<?=base_url?>proyecto/eliminar&id=<?=$pro->id?>" class="boton boton-red">Eliminar</a>
                </td>

            </tr>
        <?php endwhile; ?>

    </table>
</div>

</div>




<!--

<div id="formulario">
    <form action="<?=base_url?>usuario/save" method="POST">

        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" />
        
        <label for="apellidos">Apellidos</label>
        <input type="text" name="apellidos" />
        
        <label for="email">Email</label>
        <input type="email" name="email" />

        <label for="password">Contraseña</label>
        <input type="password" name="password" />
                
        <label for="rol">Permisos</label>
        <select name="rol">
            <option value="basic">Básico</option>
            <option value="admin">Administrador</option>
        </select>
        
        <input type="submit" name="submit" value="Registrar" />

    </form>

</div>        

-->

