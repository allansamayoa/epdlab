<!-- **********************  SECCION PARA ADMINS  ****************************** -->

<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "admin") : ?>
    <div class="ProTitulo">
        <h1>Gestionar Proyectos</h1>
        <a href="<?=base_url?>proyecto/crear" class="boton boton-peque">Crear Nuevo Proyecto</a>
    </div>
    <div class="content-hijo">

        <!-- ALERTA DE CREADO -->
        <?php if(isset($_SESSION['proyecto']) && $_SESSION['proyecto'] == 'complete'): ?>
            <strong class="alerta alerta-exito">Se ha CREADO el proyecto correctamente.</strong>
            <script>alertify.success('Se ha CREADO el proyecto correctamente.');</script>
        <?php elseif(isset($_SESSION['proyecto']) && $_SESSION['proyecto'] == 'failed'): ?>
            <strong class="alerta alerta-error">No se ha logrado CREAR el proyecto.</strong>
        <?php endif; ?>
        <?php Utilities::deleteSession('proyecto'); ?>

        <!-- ALERTA DE BORRADO -->
        <?php if(isset($_SESSION['delete']) && $_SESSION['delete'] == 'complete'): ?>
            <strong class="alerta alerta-exito">Se ha borrado el proyecto correctamente de la base de datos.</strong>
            <script>alertify.success('Se ha borrado el Proyecto');</script>
        <?php elseif(isset($_SESSION['delete']) && $_SESSION['delete'] == 'failed'): ?>
            <strong class="alerta alerta-error">No se ha logrado borrar el proyecto.</strong>
        <?php endif; ?>
        <?php Utilities::deleteSession('delete'); ?>
        <?php if($proyectos->num_rows != 0) : ?>
            <div class="data-table">
                <table border="1">
                    <tr>
                        <th>NOMBRE</th>
                        <th>CIUDAD</th>
                        <th>CLIENTE</th>
                        <th>CONTACTO</th>
                        <th>DESCRIPCION</th>
                        <th>FECHA</th>
                        <th>OPCIONES</th>
                    </tr>
                    <?php while($pro = $proyectos->fetch_object()) :?>
                        <tr>
                            <td>
                                <a href="<?=base_url?>proyecto/show&id=<?=$pro->id?>"><?=$pro->nombre;?></a>
                            </td>
                            <td><?=$pro->ciudad;?></td>
                            <?php $clientes = Utilities::showClientes();?>
                            <?php $clie = $clientes->fetch_object() ; ?>
                            <?php
                            $clide = $pro->cliente_id;
                            $clien = Utilities::showSelectedCliente($clide);
                            ?>
                            <td><?=$clien->empresa;?></td>
                            <td><?=$clien->nombre;?></td>
                            <td><?=$pro->descripcion;?></td>
                            <td><?=$pro->fecha_crea;?></td>
                            <td><a href="<?=base_url?>proyecto/editar&id=<?=$pro->id?>" class="action action-blue" >Editar</a>
                            <a href="#" onclick="preguntar(<?=$pro->id?>)" class="action action-red">Eliminar</a>
                            </td>

                        </tr>
                    <?php endwhile; ?>
                </table>
            </div>
        <?php else : ?>  
            <h3>"Aún no hay PROYECTOS AGREGADOS."</h3>
            <p>Haga click en <a href="<?=base_url?>proyecto/crear" class="boton boton-blue">Crear Nuevo Proyecto</a></p>
        <?php endif; ?>
    </div>
<?php endif; ?>



<!--

<div id="formulario">
    <form action="<?=base_url?>usuario/save" method="POST">

        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" />
        
        <label for="apellidos">Apellidos</label>
        <input type="text" name="apellidos" />
        
        <label for="email">Email</label>
        <input type="email" name="email" />

        <label for="password">Contraseña</label>
        <input type="password" name="password" />
                
        <label for="rol">Permisos</label>
        <select name="rol">
            <option value="basic">Básico</option>
            <option value="admin">Administrador</option>
        </select>
        
        <input type="submit" name="submit" value="Registrar" />

    </form>

</div>        

-->



<!-- **********************  SECCION PARA CLIENTES  ****************************** -->

<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "cliente") : ?>

    <div class="ProTitulo">

    <h1>Mis Proyectos</h1>
    </div>
    <div class="content-hijo">

        <table border="1">
            <tr>
                <th>NOMBRE</th>
                <th>CIUDAD</th>
                <th>EMPRESA</th>
                <th>CONTACTO</th>
                <th>DESCRIPCION</th>
                <th>FECHA</th>
 
            </tr>
            <!-- <//?php $proyectos = proyectoController::clienteVer();?> -->
            <?php while($pro = $proyectos->fetch_object()) : ?>
                <tr>
                    <td>
                        <a href="<?=base_url?>proyecto/show&id=<?=$pro->id?>"><?=$pro->nombre;?></a>
                    </td>
                    <td><?=$pro->ciudad;?></td>
                    <?php $clientes = Utilities::showClientes();?>
                    <?php $clie = $clientes->fetch_object() ; ?>
                    <?php
                    $clide = $pro->cliente_id;
                    $clien = Utilities::showSelectedCliente($clide);
                    ?>
                    <td><?=$clien->empresa;?></td>
                    <td><?=$clien->nombre;?></td>
                    <td><?=$pro->descripcion;?></td>
                    <td><?=$pro->fecha_crea;?></td>

                </tr>
            <?php endwhile; ?>

        </table>
    </div>
<?php endif; ?>



<!--

<div id="formulario">
    <form action="<?=base_url?>usuario/save" method="POST">

        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" />
        
        <label for="apellidos">Apellidos</label>
        <input type="text" name="apellidos" />
        
        <label for="email">Email</label>
        <input type="email" name="email" />

        <label for="password">Contraseña</label>
        <input type="password" name="password" />
                
        <label for="rol">Permisos</label>
        <select name="rol">
            <option value="basic">Básico</option>
            <option value="admin">Administrador</option>
        </select>
        
        <input type="submit" name="submit" value="Registrar" />

    </form>

</div>        



<script>
function preguntar(id)
{
    if(confirm("¿Está seguro que desea borrar el PROYECTO? Esto no se puede revertir. Al Borrar el Proyecto se borrarán todas sus dependencias de la base de datos, como EQUIPOS, PRUEBAS e INFORMES. ¿Está Seguro?"))
    {
        window.location.href = "<?=base_url?>proyecto/eliminar&id="+id;
    }
}

</script>

-->
<script type="text/javascript">
		

		function preguntar(id){
			var confirm = alertify.confirm('¿Está seguro que desea borrar el PROYECTO?',' Esto no se puede revertir. Al Borrar el Proyecto se borrarán todas sus dependencias de la base de datos, como EQUIPOS, PRUEBAS e INFORMES. ¿Está Seguro?"',null,null).set('labels', {ok:'Confirmar BORRADO', cancel:'NO BORRAR'}); 

			//callbak al pulsar botón positivo
			confirm.set('onok', function(){
                window.location.href = "<?=base_url?>proyecto/eliminar&id="+id;
			    alertify.success('Se ha borrado el Proyecto');
			});
			//callbak al pulsar botón negativo
			confirm.set('oncancel', function(){ 
			    alertify.error('No se ha borrado nada');
			})	

		}
</script>