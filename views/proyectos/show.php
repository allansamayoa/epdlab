<!-- **********************  SECCION PARA ADMINS  ****************************** -->

<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "admin") : ?>
    <div class="ProTitulo2">
        <?php if(isset($pro)): ?>
            <h1><?=$pro->nombre?></h1>
            <p><?=$pro->descripcion?></p>

        <div class="menu-pro">
            <ul>
                <li>
                <a href="<?=base_url?>proyecto/editar&id=<?=$pro->id?>" class="boton boton-blue">Editar Proyecto</a>
                </li>
                <li>
                    <a href="<?=base_url?>equipo/agregar&proid=<?=$pro->id?>" class="boton boton-green">Agregar Equipo</a>
                </li>
                <li>
                    <a href="<?=base_url?>informe/informeByProyecto&id=<?=$id?>" class="boton boton-orange">Gestionar Informes</a>
                </li>
            </ul>
        </div>

        <?php else: ?>
            <h1>El Proyecto no existe</h1>
        <?php endif; ?>
    </div>

    <div class="content-hijo">
        <h2>Equipos del proyecto</h2>
        <?php $equipos = Utilities::showEquiposPro(); 
        //var_dump($equipos);
        //die();
        
        ?>
        <?php if($equipos->num_rows != 0) : ?>
            <?php while($equi = $equipos->fetch_object()) : ?>
                <a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>"   >    
                <div class="card">
                        
                    <?php if($equi->imagen !=null): ?>
                        <img src="<?=base_url?>uploads/images/<?=$equi->imagen?>" class="thumb"/>
                    <?php else: ?>
                        <img src="<?=base_url?>uploads/images/equipo_generico.jpg" />  
                    <?php endif; ?>
                    <a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>"   >   
                    <div class="cardintern">
                        <h4><b><a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>"   ><?=$equi->nombre;?></a></b></h4> 
                        <p><?=$equi->serie;?></p> 
                        <p><?=$equi->descripcion;?></p> 
                        <p> 
                            <a href="<?=base_url?>equipo/editar&proid=<?=$pro->id?>&id=<?=$equi->id?>" class="action action-blue">Editar</a>
                            <a href="#" onclick="preguntar(<?=$equi->id?>)" class="action action-red">Eliminar</a>
                        </p>
                    </div>
                    </a>
                </div>
                </a>
            <?php endwhile; ?>
        <?php else : ?>
            <h3>"Aún no hay Equipos en este Proyecto"</h3>
            <p>Agregue algunos equipos desde aquí <a href="<?=base_url?>equipo/agregar&proid=<?=$pro->id?>" class="boton boton-green">Agregar Equipo</a>
            </p>
        <?php endif; ?>



    </div>

    <div class="content-hijo">  
        <?php
            require_once "controllers/informeController.php";
            informeController::informeByProyecto();
            require_once "views/informes/informeByProyecto.php";
        ?>
        </table>
    </div>

<?php endif; ?>



<!-- **********************  SECCION PARA CLIENTES  ****************************** -->

<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "cliente") : ?>
    <div class="ProTitulo2">
        <?php if(isset($pro)): ?>
            <h1><?=$pro->nombre?></h1>
            <h3><?=$pro->ciudad?></h3>
            <p><?=$pro->descripcion?></p>

        <?php else: ?>
            <h1>El Proyecto no existe</h1>
        <?php endif; ?>
    </div>

    <div class="content-hijo">
    <h2>Equipos del proyecto</h2>
        <?php $equipos = Utilities::showEquiposPro(); 
        //var_dump($equipos);
        //die();
        
        ?>
        <?php if($equipos->num_rows != 0) : ?>
            <?php while($equi = $equipos->fetch_object()) : ?>
                <a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>"   >    
                <div class="card">
                        
                    <?php if($equi->imagen !=null): ?>
                        <img src="<?=base_url?>uploads/images/<?=$equi->imagen?>" class="thumb"/>
                    <?php else: ?>
                        <img src="<?=base_url?>uploads/images/equipo_generico.jpg" />  
                    <?php endif; ?>
                    <a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>"   >   
                    <div class="cardintern">
                        <h4><b><a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>"   ><?=$equi->nombre;?></a></b></h4> 
                        <p><?=$equi->serie;?></p> 
                        <p><?=$equi->descripcion;?></p> 
                        <p> 
                            <a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>" class="action action-blue">VER</a>
                        </p>
                    </div>
                    </a>
                </div>
                </a>
            <?php endwhile; ?>
        <?php else : ?>
            <h3>"Aún no hay Equipos en este Proyecto"</h3>
            <p>Muy pronto podrá ver aqui los equipos en prueba de este proyecto.</p>
        <?php endif; ?>


    <div class="content-hijo">  
        <?php
            require_once "controllers/informeController.php";
            informeController::informeByProyecto();
            require_once "views/informes/informeByProyecto.php";
        ?>
        </table>
    </div>


<?php endif; ?>

<script type="text/javascript">
		

		function preguntar(id){
			var confirm = alertify.confirm('¿Está seguro que desea borrar este EQUIPO?',' Esto no se puede revertir. Al borrar el EQUIPO se borrarán todas sus dependencias de la base de datos, como PRUEBAS e INFORMES. ¿Está Seguro?"',null,null).set('labels', {ok:'borrar', cancel:'NO BORRAR'}); 

			//callbak al pulsar botón positivo
			confirm.set('onok', function(){
                window.location.href = "<?=base_url?>equipo/eliminar&id="+id+"&proid=<?=$pro->id?>";
			    alertify.success('Se ha borrado el Proyecto');
			});
			//callbak al pulsar botón negativo
			confirm.set('oncancel', function(){ 
			    alertify.error('No se ha borrado nada');
			})	

		}
</script>