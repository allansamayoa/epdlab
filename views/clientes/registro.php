<?php if(isset($edit) ) : ?>

    <h1>Edite su Perfil de Cliente Aquí</h1>
    <?php
        $url_action = base_url."cliente/save&id=".$id;
    ?>
<?php else: ?>
    <h1>Agregue un nuevo CLIENTE Aquí</h1>
    <?php
        $url_action = base_url."cliente/save";
    ?>
<?php endif; ?>



<?php if(isset($_SESSION['register']) && $_SESSION['register'] == 'complete'): ?>
    <strong class="alerta alerta-exito">Registro Completado Correctamente</strong>
<?php elseif(isset($_SESSION['register']) && $_SESSION['register'] == 'failed'): ?>
    <strong class="alerta alerta-error">Registro Fallido, introduce bien los datos.</strong>
<?php endif; ?>
<?php Utilities::deleteSession('register'); ?>

<div id="formulario2colclean">
    <form action="<?=$url_action?>" method="POST">
        <div>
            <label for="nombre" >Nombre</label>
            <input type="text" name="nombre" value="<?=isset($cliente) && is_object($cliente) ? $cliente->nombre : '';?>" <?=!Utilities::isSU() ? 'readonly' : '';?> />
            
            <label for="apellidos">Apellidos</label>
            <input type="text" name="apellidos" value="<?=isset($cliente) && is_object($cliente) ? $cliente->apellidos : '';?>" <?=!Utilities::isSU() ? 'readonly' : '';?> />
            
            <label for="email">Email</label>
            <input type="email" name="email" value="<?=isset($cliente) && is_object($cliente) ? $cliente->email : '';?>" <?=!Utilities::isSU() ? 'readonly' : '';?> />
            
            <?php if(isset($edit) ) : ?>
                <br><br>
                <label for="password">Contraseña</label>
                <input type="password" name="password" placeholder="<?=isset($cliente) && is_object($cliente) ? 'Si deja en blanco su password será el mismo' : '';?>" />
            <?php else: ?>
                <label for="password">Contraseña</label>
                <input type="password" name="password" placeholder="<?=isset($cliente) && is_object($cliente) ? 'Si deja en blanco su password será el mismo' : '';?>" />
            <?php endif; ?>
        </div>
        
        <div>
            <label for="empresa">Empresa</label>
            <input type="text" name="empresa" value="<?=isset($cliente) && is_object($cliente) ? $cliente->empresa : '';?>"  <?=!Utilities::isSU() ? 'readonly' : '';?>/>

            <label for="ciudad">Ciudad</label>
            <input type="text" name="ciudad" value="<?=isset($cliente) && is_object($cliente) ? $cliente->ciudad : '';?>"  <?=!Utilities::isSU() ? 'readonly' : '';?>/>

            <?php if(isset($edit) ) : ?>
            <?php else: ?>
                <label for="vendedor">Vendedor</label>
                <input type="text" name="vendedor" value="<?=isset($cliente) && is_object($cliente) ? $cliente->vendedor : '';?>"/>          
            <?php endif; ?>

        <?php if(isset($edit) ) : ?>
            <input type="submit" name="submit" value="Actualizar Perfil" />
        <?php else: ?>
            <input type="submit" name="submit" value="Registrar" />
        <?php endif; ?>
        </div>      


    </form>

</div>        

