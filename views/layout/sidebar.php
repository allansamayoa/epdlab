
<!-- **********************  SECCION PARA ADMINS  ****************************** -->
<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "admin") : ?>
    <div id="hola">
        <h2 style="color: gray">Hola</h2> <h1><?=$_SESSION['identity']->nombre?> <?=$_SESSION['identity']->apellidos?></h1>
        <p style="color: gray"  >Usted está loggeado como Administrador</p>
    </div>

        <nav>
            <div id="menu_log">

                <ul>
                    <li>
                        <a href="<?=base_url?>proyecto/gestionar" >Proyectos</a>
                    </li>
                    <li>
                        <a href="<?=base_url?>usuario/gestionar" >Gestionar Usuarios</a>
                    </li>
                    <li>
                        <a href="<?=base_url?>cliente/gestionar" >Gestionar Clientes</a>
                    </li>
                    <li>
                        <a href="<?=base_url?>usuario/logout" >Cerrar Sesión</a>
                    </li>
                </ul>
            </div>
        </nav>
<?php endif; ?>

<!-- **********************  SECCION PARA CLIENTES  ****************************** -->

<?php if(isset($_SESSION['identity']) && ($_SESSION['identity']->rol) == "cliente") : ?>
    <?php
        $clid = $_SESSION['identity']->id;
    ?>
    <div>
        <h2 style="color: gray">Hola </h2>
        <h1><?=$_SESSION['identity']->nombre?> <?=$_SESSION['identity']->apellidos?></h1>
        <!--p style="color: gray">Usted está loggeado como Cliente</p><br--> 

    </div>
        <nav>
            <div id="menu_log">
                <!-- Botones -->
                <ul>
                    <li>
                        <a href="<?=base_url?>proyecto/clientever" >Ver Proyectos</a>
                    </li>
                    <li>
                        <a href="<?=base_url?>cliente/editar&id=<?=$clid?>" >Actualizar mi perfil</a>
                    </li>
                    <li>
                        <a href="<?=base_url?>usuario/logout" >Cerrar Sesión</a>
                    </li>
                </ul>
            </div>
        </nav>
<?php endif; ?>
    

