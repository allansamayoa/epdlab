<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pruebas de Laboratorio</title>
   
    <link REL=StyleSheet HREF="css/normalize.css" TYPE="text/css" MEDIA=screen>
    <link rel="stylesheet" href="<?=base_url?>plugins/alertifyJS/css/alertify.min.css" />
	<link rel="stylesheet" href="<?=base_url?>plugins/alertifyJS/css/themes/semantic.css" />
    <script type="text/javascript" src="<?=base_url?>plugins/utilities.js"></script>

	<script src="<?=base_url?>plugins/alertifyJS/alertify.min.js"></script>
    <!-- <script src="js/modernizr-custom.js"></script> -->

    <!-- <link REL=StyleSheet HREF="css/styles.css" TYPE="text/css" MEDIA=screen> -->
    <!-- <link rel=StyleSheet href="css/bootstrap.css" TYPE="text/css" MEDIA=screen> -->

    <!-- <link REL=StyleSheet HREF="<?=base_url?>css/new_style.css" TYPE="text/css"> -->

    <link REL=StyleSheet HREF="<?=base_url?>css/stylesoft.css" TYPE="text/css">

</head>
<body>
    <header>
        <div id="logoname">
        <a href="<?=base_url?>"><img src="<?=base_url?>img/logo_oillab_neg_color.png"/>  </a>
        </div>

        <div class="clearfix"></div>
    </header>
    
    <div class="container"><!-- Inicio de contenedor -->
    <div id="descripcion">
            <p>Bienvenido al apartado de Pruebas de Laboratorio de <img class="img_text" src="<?=base_url?>img/logo_energiapd_transp.png"/></p>
        </div>