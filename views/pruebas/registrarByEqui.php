<div class="ProTitulo3">

    <?php
    if($_GET['proid']){
        $proid = $_GET['proid'];
    }
    ?>  
    <?php
    if($_GET['equid']){
        $equid = $_GET['equid'];
    }
    ?>  
    
    <?php if(isset($edit) ) : ?>
    <h3>EDITAR ESTA PRUEBA DEL EQUIPO:</h3>
        <?php $equi = Utilities::showSelectedEquipo($equid); ?>
        <h1><a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>"><?=$equi->nombre?></a></h1>
            
                <?php if($equi->imagen !=null): ?>
                    <img src="<?=base_url?>uploads/images/<?=$equi->imagen?>" class="thumb"/>
                <?php else: ?>
                <img src="<?=base_url?>uploads/images/equipo_generico.jpg" />  
            <?php endif; ?>
            <?php
                $url_action = base_url."prueba/saveByEquipo&id=".$id."&equid=".$equid."&proid=".$proid
            ?>

    <?php else: ?>

        <h3>REGISTRAR UNA NUEVA PRUEBA AL EQUIPO:</h3>
        <?php $equi = Utilities::showSelectedEquipo($equid); ?>
        <h1><a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>"><?=$equi->nombre?></a></h1>
            
                <?php if($equi->imagen !=null): ?>
                    <img src="<?=base_url?>uploads/images/<?=$equi->imagen?>" class="thumb"/>
                <?php else: ?>
                <img src="<?=base_url?>uploads/images/equipo_generico.jpg" />  
            <?php endif; ?>
            <?php
                $url_action = base_url."prueba/saveByEquipo&equid=".$equid."&proid=".$proid
            ?>
    <?php endif; ?>
</div>
<!--
    private $id;
        private $equipo_id;
        private $nombre;
    private $abreviacion;
    private $tipo_prueba;
    private $resultado;
    private $fecha_prueba;
    private $archivo;

-->
<div id="formulario2col">

    <form action="<?=$url_action?>" method="POST" class="formulario" enctype="multipart/form-data">
                
        <h2><label for="tipo">Tipo de Prueba</label></h2>
        <?php if(isset($edit) ) : ?>
            <?php     
                if($prueba->tipo_prueba == 1){
                    echo "<img src=".base_url."img/electrica.png><br>";
                    echo "Prueba Actual: Eléctrica";
                }else{
                    echo "<img src=".base_url."img/aceite.png><br>";
                    echo "Prueba Actual: Al Aceite";
                }
            ?>   
            <h1>DE: <?=$prueba->nombre?></h1>

            <h3><input class="elec_oculto" type="radio" name="tipo" value="1" id="elec_button" onclick="showHideElec()" <?=isset($prueba) && is_object($prueba) && $prueba->tipo_prueba == 1 ? 'checked' : '';?>> 
                <input class="ace_oculto" type="radio" name="tipo" value="2" id="ace_button" oninput="showHideAce()" <?=isset($prueba) && is_object($prueba) && $prueba->tipo_prueba == 2 ? 'checked' : '';?>>   </h3>  
        <?php else: ?>
            <p>Selecciones el tipo de prueba a registrar:</p>
            <h3><input type="radio" name="tipo" value="1" id="elec_button" onclick="showHideElec()" > Prueba Eléctrica
                <input type="radio" name="tipo" value="2" id="ace_button" oninput="showHideAce()" > Prueba al Aceite  </h3>  
        <?php endif; ?>

    
        <div class="form-select">
        <?php $cat_pruebas = Utilities::getCatPruebasByType(1);?>
            <img src="<?=base_url?>img/electrica.png" alt="Pruebas Electricas" class="elec_oculto" id="elec_image">
            <label for="nombre_elec" id="electricas" class="elec_oculto">Nombre de Prueba <strong>Electrica</strong></label>
            <select type="text" name="nombre_elec" class="elec_oculto"/>
                <?php while($cp = $cat_pruebas->fetch_object()) : ?>
        
                    <option value="<?=$cp->id;?>"><?=$cp->nombre;?>+<?=$cp->abreviacion;?>+<?=$cp->id;?></option>

                <?php endwhile; ?>
            </select>
        <div>

        <div class="form-select" id="aceite" class="ace_oculto">
        <?php $cat_pruebas = Utilities::getCatPruebasByType(2);?>
            <img src="<?=base_url?>img/aceite.png" alt="Pruebas al aceite" class="ace_oculto" id="ace_image">
            <label for="nombre_ace" class="ace_oculto">Nombre de Prueba al <strong>Aceite</strong></label>
            <select type="text" name="nombre_ace" class="ace_oculto"/>
                <?php while($cp = $cat_pruebas->fetch_object()) : ?>
            
                    <option value="<?=$cp->id;?>"><?=$cp->nombre;?>+<?=$cp->abreviacion;?>+<?=$cp->id;?></option>

                <?php endwhile; ?>
            </select>
        <div>

        <h2><label for="fecha_prueba">Fecha de la Prueba</label></h2>
        <p>Seleccione la fecha en la que se REALIZÓ la prueba... por defecto está la fecha de hoy.  </p>
        <input type="date" name="fecha_prueba" value="<?=isset($prueba) && is_object($prueba) ? $prueba->fecha_prueba : date('Y-m-d') ;?>" />

            <h3><label for="resultado">Resultado de la prueba</label></h3>
            <div>
                <input type="radio" class="input-hidden" id="elect3" name="resultado" value="3" <?=isset($prueba) && is_object($prueba) && $prueba->resultado == 3 ? 'checked' : '';?>>
                <label for="elect3" class="label-check" title="Aceptable">
                  
                  <img 
                    src="<?=base_url?>img/aceptable.png" 
                  />
                </label >
                <input type="radio" name="resultado" class="input-hidden" id="elect2" value="2" <?=isset($prueba) && is_object($prueba) && $prueba->resultado == 2 ? 'checked' : '';?>> 
                <label for="elect2" class="label-check" title="Custionable">
                  <img 
                    src="<?=base_url?>img/cuestionable.png" 
                  />
                </label>
                <input type="radio" name="resultado" class="input-hidden" id="elect1" value="1" <?=isset($prueba) && is_object($prueba) && $prueba->resultado == 1 ? 'checked' : '';?>> 
                <label for="elect1" class="label-check" title="Crítico">
                  <img 
                    src="<?=base_url?>img/critico.png" 
                  />
                </label>
            </div>



            <h3><label for="archivo">Recomendaciones</label></h3>
            <textarea name="recomend" rows="10" cols="30" placeholder=" Ingrese sus recomendaciones"><?=isset($prueba) && is_object($prueba) ? $prueba->recomend : '';?></textarea>

            <?php if(isset($prueba) && is_object($prueba) && !empty($prueba->archivo)): ?>
                <h4>Archivo Actual: <a href="<?=base_url?>uploads/pruebas/<?=$prueba->archivo?>" download="<?=$prueba->archivo;?>"><?=$prueba->archivo?></a></h4>    
                <h3><label for="archivo">CAMBIAR Archivo PDF de la PRUEBA</label></h3>
            <?php else: ?>
                <h3><label for="archivo">Subir Archivo PDF de la PRUEBA</label></h3>
            <?php endif; ?>

            
            <input type="file" name="archivo" accept="application/pdf" >

            <?php if(isset($edit) ) : ?>
                <input type="submit" value="Actualizar la Prueba" />
            <?php else: ?>
                <input type="submit" value="Registrar la Prueba" />
            <?php endif; ?>

        
    </form>
    <script>

        function showHideElec(){
            var radio = document.getElementById('elec_button');
            var elec_ocultos = document.getElementsByClassName('elec_oculto');
            var ace_ocultos = document.getElementsByClassName('ace_oculto');

            for(var i = 0; i != elec_ocultos.length; i++){
                if(radio.checked){
                    elec_ocultos[i].style.display = "block";
                    ace_ocultos[i].style.display = "none";
                }
                else{
                    elec_ocultos[i].style.display = "none";
                }
            }
        }

        function showHideAce(){
            var radio = document.getElementById('ace_button');
            var elec_ocultos = document.getElementsByClassName('elec_oculto');
            var ace_ocultos = document.getElementsByClassName('ace_oculto');

            for(var i = 0; i != ace_ocultos.length; i++){
                if(radio.checked){
                    ace_ocultos[i].style.display = "block";
                    elec_ocultos[i].style.display = "none";
                }
                else{
                    ace_ocultos[i].style.display = "none";
                }
            }
        }
        </script>
</div>



