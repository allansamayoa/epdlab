<h1>Gestion de Pruebas por Equipo</h1>

<?php if($_GET['id']){
 $id = $_GET['id'];
}
?>

<a href="<?=base_url?>informe/crear&id=<?=$id?>" class="boton boton-peque">
Crear un nuevo informe para este proyecto.
</a>

<div class="data-table">
    <table border="1">
        <tr>
            <th>EQUIPO</th>
            <th>SERIE</th>
            <th>FECHA INF</th>
            <th>RESULT ELEC</th>
            <th>RECOM ELEC</th>
            <th>RESULT ACEITE</th>
            <th>RECOM ACEITE</th>
            <th>ARCHIVO </th>
        </tr>
        <?php while($infor = $informes->fetch_object()) : ?>
        <?php //var_dump($infor); die(); ?>
            <?php
                $equide = $infor->equipo_id;
                $equi = Utilities::showSelectedEquipo($equide);
            ?>
            <tr>
                <td><?=$equi->nombre;?>
                </td>
                <td><?=$infor->serie;?></td>
                <td><?=$infor->fecha_informe;?></td>
                <td>
                    <?php     
                        if($infor->result_electricas == 1){
                            echo "<img src=".base_url."img/critico.png>";
                        } elseif ($infor->result_electricas == 2) {
                            echo "<img src=".base_url."img/cuestionable.png>";
                        } else {
                            echo "<img src=".base_url."img/aceptable.png>";
                        }
                    ?>
                </td>
                <td><?=$infor->recom_electricas;?></td>
                <td>
                <?php     
                        if($infor->result_aceite == 1){
                            echo "<img src=".base_url."img/critico.png>";
                        } elseif ($infor->result_aceite == 2) {
                            echo "<img src=".base_url."img/cuestionable.png>";
                        } else {
                            echo "<img src=".base_url."img/aceptable.png>";
                        }
                    ?>
                </td>
                <td><?=$infor->recom_aceite;?></td>
                <?php if(empty($infor->informe) || $infor->informe=='' ){?>
                <td>
                    <a href="#" class="boton boton-orange">
                    Sin Archivo
                    </a>
                </td>
                <?php }else{?>
                <td>
                    <a href="<?=base_url?>uploads/informes/<?=$infor->informe;?>" class="boton boton-orange">
                    <?=$infor->informe;?>
                    </a>
                </td>
                <?php }?>
            </tr>
        <?php endwhile; ?>

    </table>
</div>



