<div class="ProTitulo">
<h3>REGISTRAR UNA NUEVA PRUEBA EN:</h3>
<?php $proyecto = Utilities::showCurrentProyect(); ?>
<h1><?=$proyecto->nombre?></h1>
<p>Seleccione el Equipo de este Proyecto al que desea registrarle una prueba</p>
</div>

<div class="content">

<?php
    if($_GET['id']){
        $id = $_GET['id'];
    }
?>
    <form action="<?=base_url?>prueba/save&id=<?=$id?>" method="POST" class="formulario" enctype="multipart/form-data">
        
        <h2><label for="equipo">Equipo</label></h2>
            <?php $equipos = Utilities::showEquiposPro(); ?>
            <select name="equipo">
                <?php while($equi = $equipos->fetch_object()): ?>
                    <option value="<?=$equi->id?>"> 
                        <?=$equi->nombre?>
                    </option>
                <?php endwhile; ?>
            </select>
    
            <h2><label for="tipo">Tipo de Prueba</label></h2>
        <p>Selecciones el tipo de prueba a registrar:</p>
        <h3><input type="radio" name="tipo" value="1" id="elec_button" onclick="showHideElec()"> Prueba Eléctrica
            <input type="radio" name="tipo" value="2" id="ace_button" onclick="showHideAce()"> Prueba al Aceite </h3>  
    
        <div class="form-select">
            <img src="<?=base_url?>img/electrica.png" alt="Pruebas Electricas" class="elec_oculto" id="elec_image">
            <label for="nombre_elec" id="electricas" class="elec_oculto">Nombre de Prueba <strong>Electrica</strong></label>
            <select type="text" name="nombre_elec" class="elec_oculto"/>
                <option value="RESISTENCIA DE AISLAMIENTO">RESISTENCIA DE AISLAMIENTO</option>
                <option value="RESISTENCIA DE AISLAMIENTO">FACTOR DE POTENCIA</option>
                <option value="RESISTENCIA DE AISLAMIENTO">CORRIENTE EXCITACIÓN</option>
                <option value="RESISTENCIA DE AISLAMIENTO">RESISTENCIA DE DEVANADO</option>
                <option value="RELACION DE TRANSFORMACION ">RELACION DE TRANSFORMACION </option>
                <option value="IMPEDANCIA DE CORTO CIRCUITO">IMPEDANCIA DE CORTO CIRCUITO</option>
                <option value="RESPUESTA AL BARRIDO DE LA FRECUENCIA ">RESPUESTA AL BARRIDO DE LA FRECUENCIA </option>
                <option value="RIGIDEZ DIELECTRICA">RIGIDEZ DIELECTRICA</option>
                <option value="PUNTO DE ROCIO">PUNTO DE ROCIO</option>
                <option value="TIP UP">TIP UP</option>
                <option value="SATURACION">SATURACION</option>
                <option value="RESISTENCIA OHMICA DE CONTACTOS">RESISTENCIA OHMICA DE CONTACTOS</option>
                <option value="TIEMPO DE PERTURA Y CIERRE">TIEMPO DE PERTURA Y CIERRE</option>
                <option value="EXAFLORURO DE AZUFRE">EXAFLORURO DE AZUFRE</option>
                <option value="TERMOGRAFIA">TERMOGRAFIA</option>
                <option value="MUY BAJA FRECUENCIA">MUY BAJA FRECUENCIA</option>
                <option value="RESPUESTA DIELECTRICA EN EL DOMINIO DE LA FRECUENCIA">RESPUESTA DIELECTRICA EN EL DOMINIO DE LA FRECUENCIA</option>
                <option value="POLRIDAD">POLRIDAD</option>
                <option value="BURDEN">BURDEN</option>
                <option value="INYECCION SECUNDARIA">INYECCION SECUNDARIA</option>
            </select>
        <div>

        <div class="form-select" id="aceite" class="ace_oculto">
            <img src="<?=base_url?>img/aceite.png" alt="Pruebas al aceite" class="ace_oculto" id="ace_image">
            <label for="nombre_ace" class="ace_oculto">Nombre de Prueba al <strong>Aceite</strong></label>
            <select type="text" name="nombre_ace" class="ace_oculto"/>
                <option value="DGA-CAMPO">DGA-CAMPO</option>
                <option value="RIGIDEZ DIELECTRICA">RIGIDEZ DIELECTRICA</option>
                <option value="KARL FISHER">KARL FISCHER</option>
                <option value="FP LIQUIDO">FACTOR DE POTENCIA LIQUIDO</option>
                <option value="INDICE DE ACIDEZ">INDICE DE ACIDEZ</option>
                <option value="COLO E INSPECCION VISUAL">COLO E INSPECCION VISUAL</option>
                <option value="COMPUESTOS FURANOS">COMPUESTOS FURANOS</option>
                <option value="TENSION INTERFACIAL">TENSION INTERFACIAL</option>
                <option value="DENSIDAD RELATIVA">DENSIDAD RELATIVA</option>
                <option value="CONTENIDO INHIBIDOS OXIDACION">CONTENIDO DE INHIBIDOR DE OXIDACION</option>
                <option value="METALES DISUELTOS">METALES DISUELTOS</option>
                <option value="CONTENIDO PCBs">CONTENIDO PCBs</option>
                <option value="CONTENIDO AZUFRE CORROSIVO">CONTENIDO AZUFRE CORROSIVO</option>
                <option value="VISCOSIDAD">VISCOSIDAD</option>
                <option value="PUNTO DE ANILINA">PUNTO DE ANILINA</option>
            </select>
        <div>

        <h2><label for="fecha_prueba">Fecha de la Prueba</label></h2>
        <p>Seleccione la fecha en la que se REALIZÓ la prueba... por defecto está la fecha de hoy.  </p>
        <input type="date" name="fecha_prueba" value="<?php echo date("Y-m-d");?>" />

        <div class="form-check">
            <h3><label for="resultado">Resultado de la prueba</label></h3>
            <input class="form-check-input" type="radio" name="resultado" value="3" checked>Aceptable<br>
            <input class="form-check-input" type="radio" name="resultado" value="2"> Custionable<br>
            <input class="form-check-input" type="radio" name="resultado" value="1"> Crítico
        </div>

        <div class="form-textarea">
            <h3><label for="archivo">Recomendaciones</label></h3>
            <textarea name="recomend" rows="10" cols="30">
            De acuerdo a los resultados se recomienda: 
            </textarea>
        </div>

        <div class="form-check">
            <h3><label for="archivo">Subir Archivo PDF de la PRUEBA</label></h3>
            <input type="file" name="archivo" accept="application/pdf" >
        </div>

        <input type="submit" value="Registrar la Prueba" />
        
    </form>
    <script>

        function showHideElec(){
            var radio = document.getElementById('elec_button');
            var elec_ocultos = document.getElementsByClassName('elec_oculto');
            var ace_ocultos = document.getElementsByClassName('ace_oculto');

            for(var i = 0; i != elec_ocultos.length; i++){
                if(radio.checked){
                    elec_ocultos[i].style.display = "block";
                    ace_ocultos[i].style.display = "none";
                }
                else{
                    elec_ocultos[i].style.display = "none";
                }
            }
        }

        function showHideAce(){
            var radio = document.getElementById('ace_button');
            var elec_ocultos = document.getElementsByClassName('elec_oculto');
            var ace_ocultos = document.getElementsByClassName('ace_oculto');

            for(var i = 0; i != ace_ocultos.length; i++){
                if(radio.checked){
                    ace_ocultos[i].style.display = "block";
                    elec_ocultos[i].style.display = "none";
                }
                else{
                    ace_ocultos[i].style.display = "none";
                }
            }
        }
    </script>
</div>



