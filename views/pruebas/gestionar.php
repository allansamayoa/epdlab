<h1>Gestion de Pruebas Eléctricas y al Aceite</h1>

<a href="<?=base_url?>prueba/registrar" class="boton boton-peque">
Registrar una nueva prueba a este equipo.
</a>
<div class="data-table">
    <table border="1">
        <tr>
            <th>NOMBRE</th>
            <th>ABREV</th>
            <th>TIPO</th>
            <th>RESULTADO</th>
            <th>FECHA</th>
            <th>ARCHIVO</th>
            <th>ACCION</th>
        </tr>
        <?php while($pru = $pruebas->fetch_object()) : ?>
            <tr>
                <td>
                    <a href="<?=base_url?>equipo/ver&=<?=$pru->id?>"   ><?=$pru->nombre;?></a>
                </td>
                <td><?=$pru->abreviatura;?></td>
                <td>
                    <?php     
                        if($pru->tipo_prueba == 1){
                            echo "<img src=".base_url."img/electrica.png>";
                        } else {
                            echo "<img src=".base_url."img/aceite.png>";
                        }
                    ?>   
                </td>
                <td>
                    <?php     
                        if($pru->resultado == 1){
                            echo "<img src=".base_url."img/critico.png>";
                        } elseif ($pru->resultado == 2) {
                            echo "<img src=".base_url."img/cuestionable.png>";
                        } else {
                            echo "<img src=".base_url."img/aceptable.png>";
                        }
                    ?>           
                </td>
                <td><?=$pru->fecha_prueba;?></td>
                <td><?=$pru->archivo;?></td>
                <td>
                    <a href="<?=base_url?>prueba/editar&id=<?=$pru->id?>" class="boton boton-blue">Editar</a>
                    <a href="<?=base_url?>prueba/eliminar&id=<?=$pru->id?>" class="boton boton-red">Eliminar</a>
                </td>
            </tr>
        <?php endwhile; ?>

    </table>
</div>