function check_test (divID,fileID) {
	var ck1 = document.getElementById('chk_PE');
	var ck2 = document.getElementById('chk_PA');
	var div = document.getElementById(divID);
	var file = document.getElementById(fileID);
	valid_Check();
	if (ck1.checked && ck2.checked) {
		file.parentElement.style.columnCount = '2';
		div.parentElement.style.columnCount = '2';
	}else if ((ck1.checked && ck2.checked==false) || (ck1.checked==false && ck2.checked) ) {
		file.parentElement.style.columnCount = '1';
		div.parentElement.style.columnCount = '1';
	}
	div.classList.toggle("form-inactive");
	file.classList.toggle("form-inactive");
}

function valid_Check(){
	var ck1 = document.getElementById('chk_PE');
	var ck2 = document.getElementById('chk_PA');
	if (ck1.checked && ck2.checked) {
		ck1.disabled=false;
		ck2.disabled=false;
	}else if (ck1.checked && ck2.checked==false) {
		ck1.disabled=true;
		ck2.disabled=false;
		ck1.disabled=true;
	}else if (ck1.checked==false && ck2.checked) {
		ck1.disabled=false;
		ck2.disabled=true;
	}
}