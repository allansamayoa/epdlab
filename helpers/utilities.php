<?php

class Utilities{

    public static function deleteSession($name){
        if(isset($_SESSION[$name])){
            $_SESSION[$name] = null;
            unset($_SESSION[$name]);
        }
        return $name;
    }

    public static function isAdmin(){
        if(!isset($_SESSION['identity']) || ($_SESSION['identity']->rol) != "admin"){
            header("Location:".base_url);
        }else{
            return true;
        }
    }

    public static function isSU(){ //valida si es administrador o no, 
        if(!isset($_SESSION['identity']) || ($_SESSION['identity']->rol) != "admin"){
            return false;
        }else{
            return true;
        }
    }

    public static function showClientes(){
        require_once 'models/cliente.php';
        $cliente = new Cliente();
        $clientes = $cliente->getAll();
        return $clientes;
    }

    public static function showProyectos(){
        require_once 'models/proyecto.php';
        $proyecto = new Proyecto();
        $proyectos = $proyecto->getAll();
        return $proyectos;
    }

    public static function showProyectosByAdminId(){
        $admin_id = $_SESSION['identity']->id;
        require_once 'models/proyecto.php';
       
        $proyecto = new Proyecto();
        $proyecto->setAdmin_id($admin_id);
        $proyectos = $proyecto->getSomeByAdminId();
        return $proyectos;
    }

    
    public static function showEquiposPro(){

        if(isset($_GET['id'])){
        require_once 'models/equipo.php';
        $proyecto_id = $_GET['id'];
        
        $equipo = new Equipo();
        $equipo->setProyecto_id($proyecto_id);
        $equipos = $equipo->getSomeByProyectoId();
        return $equipos;
        }
    }

    public static function showEquiposProid(){

        if(isset($_GET['proid'])){
        require_once 'models/equipo.php';
        $proyecto_id = $_GET['proid'];
        
        $equipo = new Equipo();
        $equipo->setProyecto_id($proyecto_id);
        $equipos = $equipo->getSomeByProyectoId();
        return $equipos;
        }
    }

    public static function showEquiposInfor(){

        if(isset($_GET['proid'])){
        require_once 'models/equipo.php';
        $proyecto_id = $_GET['proid'];
        
        $equipo = new Equipo();
        $equipo->setProyecto_id($proyecto_id);
        $equipos = $equipo->getSomeByProyectoId();
        return $equipos;
        }
    }

    public static function showCurrentProyect(){

        if(isset($_GET['id'])){
        require_once 'models/proyecto.php';
        $proyecto_id = $_GET['id'];
        
        $proyecto = new Proyecto();
        $proyecto->setId($proyecto_id);
        $pro = $proyecto->getOneById();
        return $pro;
        }
    }

    public static function showCurrentProyectEqui(){

        if(isset($_GET['proid'])){
        require_once 'models/proyecto.php';
        $proyecto_id = $_GET['proid'];
        
        $proyecto = new Proyecto();
        $proyecto->setId($proyecto_id);
        $pro = $proyecto->getOneById();
        return $pro;
        }
    }


    public static function showCurrentProyectProid(){

        if(isset($_GET['proid'])){
        require_once 'models/proyecto.php';
        $proyecto_id = $_GET['proid'];
        
        $proyecto = new Proyecto();
        $proyecto->setId($proyecto_id);
        $pro = $proyecto->getOneById();
        return $pro;
        }
    }

    public static function showCurrentProid(){

        if(isset($_GET['proid'])){
        require_once 'models/proyecto.php';
        $proyecto_id = $_GET['proid'];
        
        $proyecto = new Proyecto();
        $proyecto->setId($proyecto_id);
        $pro = $proyecto->getOneById();
        return $pro;
        }
    }

    public static function showSelectedEquipo($equipo_id){

        if(isset($equipo_id)){
        require_once 'models/equipo.php';
        $equide = $equipo_id;
        
        $equipo = new Equipo();
        $equipo->setId($equide);
        $equi = $equipo->getOneById();
        return $equi;
        }
    }

    public static function getPruebas(){
        require_once 'models/prueba.php';

        $prueba = new Prueba();
        $pruebas = $prueba->getAll();

        return $pruebas;
    }

    public static function getPruebasByEquipoId(){
        require_once 'models/prueba.php';
        
        $id = $_GET['id'];

        $prueba = new Prueba();
        $prueba->setEquipo_id($id);
        $pruebas = $prueba->getSomeByEquipoId();
        return $pruebas;
    }

    public static function showSelectedCliente($cliente_id){

        if(isset($cliente_id)){
        require_once 'models/cliente.php';
        $cliente = $cliente_id;
        
        $cliente = new Cliente();
        $cliente->setId($cliente_id);
        $clie = $cliente->getOneById();
        return $clie;
        }
    }

    public static function showCurrentNamePrueba($id){
        require_once 'models/catalog_pruebas.php';
        $cprueba_id = $id;
        
        $cprueba = new catalog_prueba();
        $cprueba->setId($cprueba_id);
        $cpru = $cprueba->getOneById();
        return $cpru;
    }

    public static function getCatPruebas(){
        require_once 'models/catalog_pruebas.php';

        $catprueba = new catalog_prueba();
        $cat_pruebas = $catprueba->getAll();

        return $cat_pruebas;
    }

    public static function getCatPruebasByType($tipo){
        require_once 'models/catalog_pruebas.php';

        $catprueba = new catalog_prueba();
        $catprueba->setTipo_prueba($tipo);
        $cat_pruebas = $catprueba->getByType();

        return $cat_pruebas;
    }

    
}