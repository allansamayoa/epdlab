<?php

//SECCION PARA VER EQUIPOS DE UN PROYECTO (proyectos/show.php)


            <div id="equipos-pro">
                <table border="1">
                    <tr>
                        <th>NOMBRE</th>
                        <th>MARCA</th>
                        <th>MODELO</th>
                        <th>SERIE</th>
                        <th>OPCIONES</th>

                    </tr>
                    <?php $equipos = Utilities::showEquiposPro(); ?>
                    <?php while($equi = $equipos->fetch_object()) : ?>
                        <tr>
                            <td>
                                <a href="<?=base_url?>equipo/show&id=<?=$equi->id?>&proid=<?=$pro->id?>"   ><?=$equi->nombre;?></a>
                            </td>
                            <td><?=$equi->marca;?></td>
                            <td><?=$equi->modelo;?></td>
                            <td><?=$equi->serie;?></td>
                            <td>
                            <a href="<?=base_url?>equipo/editar&proid=<?=$pro->id?>&id=<?=$equi->id?>" class="boton boton-blue">Editar</a>
                            <a href="<?=base_url?>equipo/eliminar&id=<?=$equi->id?>&proid=<?=$pro->id?>" class="boton boton-red">Eliminar</a>
                            </td>
                        </tr>
                    <?php endwhile; ?>
                </table>
            </div>